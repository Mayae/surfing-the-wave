﻿/*************************************************************************************

	Surfing The Wave - Interactive Procedural Sound Design Game - v. 0.1.0.

	Copyright (C) 2015 Janus Lynggaard Thorborg [LightBridge Studios]

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.

*************************************************************************************/

using UnityEngine;
using System.Collections;

public interface CallbackMechanism
{
	void onCallbackEvent(EventCallback sender);
}

public class EventCallback : MonoBehaviour
{
	public GameObject obj;
	protected CallbackMechanism cb;
	// Use this for initialization
	void Awake ()
	{
		if(obj == null)
		{
			print("Warning: Callback without valid gameobject.");
		}
		else
		{
			cb = obj.GetComponent<MonoBehaviour>() as CallbackMechanism;
			if(cb == null)
			{
				print("Warning: Object to call back is not a callback mechanism.");
			}
		}
	}

}
