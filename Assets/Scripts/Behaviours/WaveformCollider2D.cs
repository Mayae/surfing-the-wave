/*************************************************************************************

	Surfing The Wave - Interactive Procedural Sound Design Game - v. 0.1.0.

	Copyright (C) 2015 Janus Lynggaard Thorborg [LightBridge Studios]

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.

*************************************************************************************/

using UnityEngine;
using System.Collections;

public class WaveformCollider2D : MonoBehaviour 
{

	public WalkableWaveform wave;
	public bool isBelow = false;
	public float timeAtLastContact;
	public float friction = 0.5f;
	public float recoil = 0.0f;
	public float ignoreMagnitudeDistance = 3.0f;
	// for display
	public Vector2 bodyVelocity;
	public float angularVelocity;
	public float fixedAngle;
	public Vector2 bodyPosition;
	public float angularDrag;
	public Vector3 transformPosition;
	public Vector3 distanceToWave;
	public float distanceToWaveMagnitude;
	public float maximumPhysicalMove = 1.0f;
	public event System.Action<WaveformCollider2D, WalkableWaveform.PhysicalInteraction> onCollisionStart;
	public event System.Action<WaveformCollider2D, WalkableWaveform.PhysicalInteraction> onCollisionEnd;
	public bool easyCollision = false;
	private Rigidbody2D body;
	private Transform tsf;
	private bool isAutomatic = true;
	private bool isColliding = false;
	void Awake () 
	{
		body = GetComponent<Rigidbody2D> ();
		tsf = GetComponent<Transform> ();
		if(body == null)
		{
			print ("Error: WaveformCollider2D needs an attached RigidBody2D!");
		}
		if(wave == null)
		{
			print ("Error: WaveformCollider2D needs a wave reference!");
		}

		Blunt.Utils.ExtendedPhysics.instance ().lateFixedUpdateCallback += doPhysics;
		timeAtLastContact = -1;
	}

	public bool getIsColliding()
	{
		return isColliding;
	}

	/// <summary>
	/// If toggle is true (default), this object will automatically move the rigidbody after Unity's own
	/// physics calculations. Override this, and call doPhysics manually if you want to control
	/// when the physics is (if at all) applied.
	/// </summary>
	/// <param name="toggle"></param>
	public void setIsUsingAutomaticCollision(bool toggle)
	{

		if(toggle)
		{
			if(!isAutomatic)
			{
				isAutomatic = true;
				Blunt.Utils.ExtendedPhysics.instance().lateFixedUpdateCallback += doPhysics;
			}
		}
		else
		{
			if(isAutomatic)
			{
				isAutomatic = false;
				Blunt.Utils.ExtendedPhysics.instance().lateFixedUpdateCallback -= doPhysics;
			}
		}
	}
	/// <summary>
	/// Runs the physics and collisions displacements + calculations.
	/// </summary>
	public void doPhysics()
	{
		if(body.isKinematic)
			return;
		bodyVelocity = body.velocity;
		angularVelocity = body.angularVelocity;
		bodyPosition = body.position;
		transformPosition = tsf.position;
		angularDrag = body.angularDrag;

		distanceToWave = tsf.localPosition - wave.transform.localPosition;
		if(distanceToWave.y < -1)
			isBelow = true;
		else
			isBelow = false;
        distanceToWave.Scale(new Vector3(1.0f / wave.transform.localScale.y, 1.0f / wave.transform.localScale.x, 0f));
		//distanceToWaveMagnitude = Mathf.Max(Mathf.Abs(distanceToWave.x), Mathf.Abs(distanceToWave.y));
		distanceToWaveMagnitude = distanceToWave.magnitude;
		if(distanceToWaveMagnitude < ignoreMagnitudeDistance)
		{
			// Due to the nature of frames and movement, we cannot ensure the target 
			// won't pass through the waveform between frames - we have to simplify
			// the logic and assume the target is either to be over or below the waveform.
			if(isBelow)
			{

			}
			else
			{
				// check collision.
				var collision = wave.collide(body.position, tsf.localScale.x, isBelow);
				if(collision.collisionOccured)
				{
					if(onCollisionStart != null)
						onCollisionStart(this, collision);
					timeAtLastContact = Time.time;

					if(easyCollision)
					{
						var cdox = /*collision.displacement.x*/ 0;
                        var newPos = new Vector2(body.position.x + cdox,
								body.position.y + collision.displacement.y);
						// the magnitude of the displacement is stored in displacement.y.
						// multiplying this by the horizontal angle gives the angular force to be added
						// to the velocity.
						body.velocity = new Vector2(body.velocity.x * friction - collision.displacement.y * collision.horizontalAngle,
													collision.displacement.y + 0.1f * -body.velocity.y * friction);
						// teleport body.
						body.position = newPos;
						// calculate some recoil/bounciness, which is 50% initial velocity and
						// 50% displacement recoil.
						var recoilX = 1 * recoil * -collision.displacement.y;
						var recoilY = 100 * recoil * -cdox;
						body.AddForce(
							new Vector2(body.velocity.x * recoilX * 0.5f + 0.5f * recoilX,
									body.velocity.y * recoilY * 0.5f + 0.5f * recoilY)
						);
					}
					else
					{ 
						float yDisplacement;
						float xDisplacement = body.position.x + collision.displacement.x * collision.displacement.y * 0.5f;

						var xVelocity = (body.velocity.x * friction);
						if(collision.horizontalAngle > 0.1)
						{
							var amount = Mathf.Exp(-collision.horizontalAngle * 10);
							yDisplacement = amount * collision.displacement.y;
							//xDisplacement -= Mathf.Min(collision.horizontalAngle * collision.displacement.y, wave.transform.localScale.x / wave.numLines);
							// subtract a lengths width
							xDisplacement -= wave.transform.localScale.x / wave.numLines;
							if(xVelocity > 0)
								xVelocity *= 0.2f;
						}
						else
							yDisplacement = collision.displacement.y;
						var newPos = new Vector2(xDisplacement,
							body.position.y + yDisplacement);
						body.position = newPos;



						body.velocity = new Vector2(xVelocity, 0.1f * -body.velocity.y * friction);
						body.AddForce(new Vector2(/*xDisplacement * 0.1f*/ 0, -yDisplacement));
						//print("horiangle: " + collision.horizontalAngle);
					}
					isColliding = true;
					if(onCollisionEnd != null)
						onCollisionEnd(this, collision);
				}
				else
				{
					isColliding = false;
				}
			}
		}

	}


}
