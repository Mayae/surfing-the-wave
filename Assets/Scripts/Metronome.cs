﻿/*************************************************************************************

	Surfing The Wave - Interactive Procedural Sound Design Game - v. 0.1.0.

	Copyright (C) 2015 Janus Lynggaard Thorborg [LightBridge Studios]

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.

*************************************************************************************/

using UnityEngine;
using System.Collections;
using Blunt;
using Blunt.Sound;
using Blunt.Synthesis;
public class Metronome : MonoBehaviour
{
	public FXComponentSynth synthToUse;

	public float ticksPerBeat = 1.0f;
	public float ticksPerUpbeat = 4.0f;
	public float upbeatFreqRatio = 1.5f;
	public float baseFrequency = 440;
	public float volumeInDBs = -9.0f;
	private SoundPrefabs.Metronome md;
	private SoundPrefabs.Metronome mu;
	private FMSynth synth;
	private Sequencer seq;
	PlayHead.TimeOffset timeAtNextPlay;
	bool isUpbeat = false;

	void onAudioLoad()
	{
		synth = synthToUse.getSynthesizer();
		md = new SoundPrefabs.Metronome(synth);
		md.initializeWithVoices(3);
		md.foreachVoice(v => { v.setPitch(baseFrequency); v.volume = MathExt.dbToFraction(volumeInDBs); });
		mu = new SoundPrefabs.Metronome(synth);
		mu.initializeWithVoices(2);
		mu.foreachVoice(v => { v.setPitch(baseFrequency * upbeatFreqRatio); v.volume = MathExt.dbToFraction(volumeInDBs); });
		seq = new Sequencer(synth);
		seq.sequenceEvery(md, PlayHead.TimeOffset.zero, PlayHead.createTimeOffset(0, 0, 0, ticksPerBeat));
		seq.sequenceEvery(mu, PlayHead.TimeOffset.zero, PlayHead.createTimeOffset(0, 0, 0, ticksPerUpbeat * ticksPerBeat));

	}



	void Start ()
	{
		GameState.instance().onAudioIsLoaded += onAudioLoad;
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
