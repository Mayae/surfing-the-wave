﻿/*************************************************************************************

	Surfing The Wave - Interactive Procedural Sound Design Game - v. 0.1.0.

	Copyright (C) 2015 Janus Lynggaard Thorborg [LightBridge Studios]

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.

*************************************************************************************/

using UnityEngine;
using System.Collections;

public class MixerTest : MonoBehaviour, Blunt.Sound.SoundStage
{
	public Blunt.Sound.Mixer mixer;
	public float frequency = 220f;
	public float vol = 1.0f;

	private float phase = 0;

	public void process (float[] data, int nSampleFrames, int channels, bool channelIsEmpty)
	{
		var rotation = 2 * Mathf.PI * frequency / Blunt.Synthesis.EnvironmentTuning.sampleRate;
		
		for(int i = 0; i < nSampleFrames * channels; i += channels)
		{
			data[i + 1] = data[i] = vol * Mathf.Sin (phase);
			phase += rotation;
			if(phase > 2 * Mathf.PI)
				phase -= 2 * Mathf.PI;
		}
	}

	void Awake ()
	{
		var channel = mixer.getChannel ("Main");
		channel.setIsEnabled (true);
		channel.addStage (this);
		mixer.setIsEnabled (true);
	}
	
}
