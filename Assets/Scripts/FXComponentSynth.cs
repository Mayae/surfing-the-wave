﻿/*************************************************************************************

	Surfing The Wave - Interactive Procedural Sound Design Game - v. 0.1.0.

	Copyright (C) 2015 Janus Lynggaard Thorborg [LightBridge Studios]

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.

*************************************************************************************/

using UnityEngine;
using System.Collections;
using Blunt;
using Blunt.Sound;
using Blunt.Synthesis;

/// <summary>
/// A synth which can be used as a monobehaviour. 
/// The synth has higher latency, since effects can be scheduled
/// at lower rates.
/// </summary>
public class FXComponentSynth : MonoBehaviour
{
	public string mixerChannelToUse = "Effects";

	public Mixer mixer;

	private Mixer.MixerChannel mixerChannel;

	private FMSynth synth;

	public FMSynth getSynthesizer()
	{
		if(synth == null)
		{
			print("FXComponentSynth: Something is trying to reach a synthesizer before audio has been loaded...");
			onAudioLoad();
		}
		return synth;
	}

	public void OnAudioFilterRead(float[] data, int n)
	{

	}

	private void onAudioLoad()
	{
		if(synth == null)
		{
			synth = new FMSynth();
			synth.setLatency(4096);
			mixerChannel = mixer.getChannel(mixerChannelToUse);
			mixerChannel.addStage(synth);
		}
	}

	private void onGameLoaded()
	{
		mixerChannel.setIsEnabled(true);
		mixer.setIsEnabled(true);
	}

	// Use this for initialization
	void Start ()
	{
		GameState.instance().onAudioLoad += onAudioLoad;
		GameState.instance().onGameIsLoaded += onGameLoaded;
	}
	
}
