﻿/*************************************************************************************

	Surfing The Wave - Interactive Procedural Sound Design Game - v. 0.1.0.

	Copyright (C) 2015 Janus Lynggaard Thorborg [LightBridge Studios]

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.

*************************************************************************************/

using UnityEngine;
using System.Collections;

public class BlowingPuzzle : MonoBehaviour, CallbackMechanism
{
	public FanController fc;
	public BallEnabler be;
	public GameObject ballKeeper;
	public GameObject massiveBall;
	// Use this for initialization



	public void onButtonFinished()
	{
		if(massiveBall.rigidbody2D != null)
		{
			massiveBall.rigidbody2D.isKinematic = false;
		}
		StartCoroutine(cameraFocus());
	}

	IEnumerator cameraFocus()
	{
		yield return new WaitForSeconds(0.5f);
		fc.startFan();
		CameraFollower.instance().lookAtAreaTemporarily(ballKeeper.transform.position, 1.5f, 5);
		yield return new WaitForSeconds(3);
		ballKeeper.SetActive(false);
		yield return new WaitForSeconds(5);
		
	}

	void onGameLoaded()
	{
		be.onButtonPress += onButtonFinished;
	}
	void onGameStart()
	{
		fc.stopFan();
		if(massiveBall.rigidbody2D != null)
		{
			massiveBall.rigidbody2D.isKinematic = true;
		}
		ballKeeper.SetActive(true);
	}

	void onButtonPress()
	{
		fc.stopFan();
	}

	public void onCallbackEvent(EventCallback e)
	{
		onPuzzleSolved();
	}

	void onPuzzleSolved()
	{
		fc.stopFan();
		if(GameState.instance().onPuzzleSolved != null)
			GameState.instance().onPuzzleSolved();
	}

	void Start()
	{
		GameState.instance().onGameIsLoaded += onGameLoaded;
		GameState.instance().onGameStart += onGameStart;
	}


}
