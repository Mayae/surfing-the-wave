﻿/*************************************************************************************

	Surfing The Wave - Interactive Procedural Sound Design Game - v. 0.1.0.

	Copyright (C) 2015 Janus Lynggaard Thorborg [LightBridge Studios]

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.

*************************************************************************************/

#define BODY
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Player : MonoBehaviour 
{
	private Transform tsf, waveTsf;
	private Rigidbody2D body;
	private List<WaveformCollider2D> wcolliders;
	public WalkableWaveform wave;
	public float jumpHeight = 5000f;
	public float jumpWindow = 0.1f;
	public float moveSpeed = 1f;
	public float maximumVelocity = 1f;
	private float oldVelocity = 1.0f;
	private bool hasJumped = false;
	private bool isTouchingWave = false;
	private bool isTouchingMap = false;
	private float lastJumpTime = 0.0f;
	private bool isGrounded = true;
	private bool isInWater;
	private bool hasEnteredPuzzleZone;
	private int numPickups;
	public float normalGravity = 0.5f;
	public float waterGravity = 0.1f;
	// Use this for initialization
	void Start () 
	{
		wcolliders = new List<WaveformCollider2D>();
		var colliders = GetComponents<WaveformCollider2D> ();
		foreach(var c in colliders)
		{
			wcolliders.Add(c);
			c.setIsUsingAutomaticCollision(false);
			c.onCollisionStart += onWaveCollisionStart;
			c.onCollisionEnd += onWaveCollisionStop;
		}
		tsf = GetComponent<Transform> ();
		waveTsf = wave.GetComponent<Transform> ();
		body = GetComponent<Rigidbody2D> ();
		// override automatic collisions, so we can enable jumping.

		GameState.instance().onPostPhysics += postGroundedNotifs;
		GameState.instance().onGameStart += onGameStart;
		GameState.instance().onPickup += onPickup;
		GameState.instance().onWaterEntry += onWaterEntry;
        GameState.instance().onWaterExit += onWaterExit;

		lastJumpTime = 0 - jumpWindow;
	}

	void onWaterExit()
	{
		if(isInWater)
		{
			isInWater = false;
			maximumVelocity = oldVelocity;
		}

	}

	void onWaterEntry()
	{
		if(!isInWater)
		{
			isInWater = true;
			oldVelocity = maximumVelocity;
			maximumVelocity *= 0.1f;
		}

	}

	void onGameStart()
	{
		isInWater = false;
		hasEnteredPuzzleZone = false;
	}

	void onPickup()
	{
	}

	int enters;

	void OnTriggerEnter2D(Collider2D c)
	{
		if(c.gameObject.tag == "Ability")
		{
			PickableAbility p = c.gameObject.GetComponent<PickableAbility>();
			if(p != null)
			{
				p.consume();
			}
		}
		else if(c.gameObject.tag == "QuietZone")
		{
			if(GameState.instance().onQuietEntry != null)
				GameState.instance().onQuietEntry();
        }
		else if(c.gameObject.tag == "PuzzleEnter")
		{
			if(!hasEnteredPuzzleZone)
			{
				hasEnteredPuzzleZone = true;
				if(GameState.instance().onPuzzleEnter != null)
					GameState.instance().onPuzzleEnter();
			}
		}
		else if(c.gameObject.tag == "Water")
		{
			body.gravityScale = waterGravity;
			if(GameState.instance().onWaterEntry != null)
				GameState.instance().onWaterEntry();
		}
		else if (c.gameObject.tag != "CameraAnimator")
		{
			enters++;
			isTouchingMap = enters > 0;
			hasJumped = false;
		}
	}
	void OnTriggerStay2D(Collider2D c)
	{
		if(c.gameObject.tag != "Ability" && c.gameObject.tag != "CameraAnimator" && c.gameObject.tag != "Water" && c.gameObject.tag != "QuietZone"
			&& c.gameObject.tag != "PuzzleEnter")
		{
			if(enters == 0)
				enters++;
			isTouchingMap = true;
			hasJumped = false;
		}
	}
	void OnTriggerExit2D(Collider2D c)
	{
		if(c.gameObject.tag == "Water")
		{
			body.gravityScale = normalGravity;
			if(GameState.instance().onWaterExit != null)
				GameState.instance().onWaterExit();
		}
		else if(c.gameObject.tag == "QuietZone")
		{
			if(GameState.instance().onQuietExit != null)
				GameState.instance().onQuietExit();
		}
		else if(c.gameObject.tag != "Ability" && c.gameObject.tag != "CameraAnimator" && c.gameObject.tag != "PuzzleEnter")
		{
			enters--;
			isTouchingMap = enters > 0;
		}
	}
	bool isAbleToJump()
	{
		if(!hasJumped)
		{
			if(Time.time > lastJumpTime + jumpWindow )
			{
				bool timeouts = false;
				foreach(var c in wcolliders)
				{
					if(c.timeAtLastContact + jumpWindow > Time.time)
					{
						timeouts = true;
						break;
					}
                }

				if(timeouts || isTouchingMap)
				{
					return true;
				}

			}
		}
		return false;
	}

	void postGroundedNotifs()
	{
		if(isAbleToJump())
		{
			if(!isGrounded)
			{
				isGrounded = true;
				GameState.instance().onGrounded();
			}
		}
		else
		{
			if(isGrounded)
			{
				isGrounded = false;
				GameState.instance().onAirborne();
			}
		}
	}

	void onWaveCollisionStart(WaveformCollider2D wave, WalkableWaveform.PhysicalInteraction c)
	{
		if(Mathf.Abs(c.horizontalAngle) < 0.1f)
			hasJumped = false;
	}
	void onWaveCollisionStop(WaveformCollider2D wave, WalkableWaveform.PhysicalInteraction c)
	{

	}

	void FixedUpdate () 
	{
		if(GameState.instance().currentGameState == GameState.State.Ingame)
		{

			float verticalAxis = 0.0f;
			bool jumpAttempt = Input.GetAxis("Vertical") > 0.0f;
			if(isAbleToJump() && jumpAttempt)
			{
				verticalAxis += jumpHeight;
				lastJumpTime = Time.time;
				hasJumped = true;
				isGrounded = false;
				GameState.instance().onJump();
			}
			else
			{
				foreach(var c in wcolliders)
					c.doPhysics();
			}


			Vector2 direction = new Vector2(Input.GetAxis("Horizontal") * 5 * Time.deltaTime * 60 * moveSpeed, verticalAxis);
			// add movement
			if(direction.magnitude > 0)
			{

				body.AddForce(direction);

			}

			// clamp velocity, grabbing the magnitude firstly:
			var velMag = body.velocity.magnitude;
			if(velMag > maximumVelocity)
			{
				// get the difference, ie. the amount of opposing force we should scalably add:
				var difference = velMag - maximumVelocity;
				// scale the normalized vector by the force, and add the force negatively.
				body.AddForce(body.velocity.normalized * -(difference) * 2);

			}
		}
	}
}
