﻿/*************************************************************************************

	Surfing The Wave - Interactive Procedural Sound Design Game - v. 0.1.0.

	Copyright (C) 2015 Janus Lynggaard Thorborg [LightBridge Studios]

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.

*************************************************************************************/

using UnityEngine;
using System.Collections;
using Blunt.UnityExt;

public class CameraFollower : MonoBehaviour
{

	public Transform tracker;
	public float yQuantization = 1f;
	private Transform tsf;
	private float velocityX, velocityY;
	private float newYValue;
	private Camera mainCamera;
	private float defaultCameraSize;
	private float cameraSizeVelocity;
	private float currentCameraSize;

	private Vector3 cameraPosition;
	bool mouseDragStarted;
	private CameraLocation fixedLook;

	public class CameraLocation
	{
		public Vector3 where;
		public float length;
		public float timeStamp;
		public float size;
		public CameraLocation(Vector3 whereToLook, float timeToLook, float cameraSize = 1.0f)
		{
			where = whereToLook;
			length = timeToLook;
			timeStamp = -1;
			size = cameraSize;
		}
	}
	private Vector3 mouseOrigin;	// Position of cursor when mouse dragging starts
	private Blunt.CFrontQueue<CameraLocation> locations = new Blunt.CFrontQueue<CameraLocation>();
	private static CameraFollower internalInstance;
	// Use this for initialization
	void Start ()
	{
		tsf = GetComponent<Transform>();
		newYValue = tsf.position.y;
		internalInstance = this;
		mainCamera = Camera.main;
		if(mainCamera.isOrthoGraphic)
			defaultCameraSize = mainCamera.orthographicSize;
		else
			defaultCameraSize = mainCamera.fieldOfView;

		mouseDragStarted = false;
	}

	public static CameraFollower instance()
	{
		return internalInstance;
	}

	public void lookAtFixed(CameraLocation cl)
	{
		fixedLook = cl;
	}

	public void removeFixedLook()
	{
		fixedLook = null;
	}

	float selectCameraSize()
	{
		float smoothedCameraSize = 1.0f;
		if(fixedLook != null)
		{
			smoothedCameraSize = fixedLook.size;
		}
		else if(locations.Count > 0)
		{
			smoothedCameraSize = locations.Peek().size;
		}
		return smoothedCameraSize;
	}

	Vector3 selectCameraLocation()
	{
		if(fixedLook != null)
		{
			return fixedLook.where;
		}
		else if(locations.Count > 0)
		{
			var front = locations.Peek();
			if(front.timeStamp == -1)
			{
				front.timeStamp = Time.time;
			}

			if(Time.time > (front.timeStamp + front.length))
			{
				locations.pop();
				return selectCameraLocation();
			}
			else
			{
				return front.where;
			}
		}
		else
		{
			return tracker.position;
		}
	}
	
	public void lookAtAreaTemporarily(Vector3 where, float size, float time)
	{
		locations.push(new CameraLocation(where, time, size));
	}

	// Update is called once per frame
	void Update ()
	{
		// rotate camera

		if(Input.GetMouseButtonDown(1))
		{
			mouseDragStarted = true;
			mouseOrigin = Input.mousePosition;
		}
		else if(Input.GetMouseButtonUp(1))
		{
			mouseDragStarted = false;
        }

		if(mouseDragStarted)
		{
			//mainCamera.transform.Rotate(Input.GetAxis("Mouse Y"), Input.GetAxis("Mouse X"), 0);
			Vector3 pos = Camera.main.ScreenToViewportPoint(Input.mousePosition - mouseOrigin);

			transform.RotateAround(transform.position, transform.right, -Input.GetAxis("Mouse Y"));
			transform.RotateAround(transform.position, Vector3.up, Input.GetAxis("Mouse X"));

			//cameraRotation.x = Mathf.Clamp(Input.GetAxis("Mouse Y") * 0.01f + cameraRotation.x, -300, 60);
			//cameraRotation.y = Mathf.Clamp(Input.GetAxis("Mouse X") * 0.01f + cameraRotation.y, -300, 60);
			//print(cameraRotation);
			//mainCamera.transform.rotation = cameraRotation;
		}

		Vector3 objectToLookAt = selectCameraLocation();

		float smoothedCameraSize = selectCameraSize();
		// smoothdamp camera size

		currentCameraSize = mainCamera.isOrthoGraphic ? mainCamera.orthographicSize : mainCamera.fieldOfView;

		currentCameraSize = Mathf.SmoothDamp(currentCameraSize, defaultCameraSize * smoothedCameraSize, ref cameraSizeVelocity, 0.9f);

		if(mainCamera.isOrthoGraphic)
		{
			mainCamera.orthographicSize = currentCameraSize;
		}
		else
		{
			mainCamera.fieldOfView = currentCameraSize;
		}

        if(Mathf.Abs(newYValue - objectToLookAt.y) > yQuantization)
		{
			newYValue = objectToLookAt.y;
		}

		float smoothedX = Mathf.SmoothDamp(tsf.position.x, objectToLookAt.x, ref velocityX, 0.7f);
		float smoothedY = Mathf.SmoothDamp(tsf.position.y, newYValue, ref velocityY, 0.3f);
        cameraPosition = new Vector3(smoothedX, smoothedY, tsf.position.z);

		tsf.position = cameraPosition;

	}
}
