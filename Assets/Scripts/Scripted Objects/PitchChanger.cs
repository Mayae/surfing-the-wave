﻿using UnityEngine;
using System.Collections;

public class PitchChanger : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update ()
	{
		if(Input.GetKeyDown(KeyCode.M))
		{
			Blunt.Synthesis.EnvironmentTuning.basePitch *= 2;
			Blunt.Synthesis.EnvironmentTuning.updateFields();
		}
		else if(Input.GetKeyDown(KeyCode.N))
		{
			Blunt.Synthesis.EnvironmentTuning.basePitch *= 0.5f;
			Blunt.Synthesis.EnvironmentTuning.updateFields();
		}
	}
}
