﻿/*************************************************************************************

	Surfing The Wave - Interactive Procedural Sound Design Game - v. 0.1.0.

	Copyright (C) 2015 Janus Lynggaard Thorborg [LightBridge Studios]

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.

*************************************************************************************/

using UnityEngine;
using System.Collections;

using Blunt.Synthesis;

public class BallEnabler : ButtonPresser
{

	public FXComponentSynth fxSynth;
	private FMSynth synth;
	private SoundPrefabs.SimpleButtonClick click;
	private SoundPrefabs.SonicBoom boom;
	private VoiceBuffer<FMSynth.Voice> reward;

	void startGame()
	{
		reset();
	}

	public override void onButtonPressed()
	{
		base.onButtonPressed();
		boom.stealOldestVoice().play(-12);
		click.stealOldestVoice().play(-3);
	}
	public override void onButtonFinished()
	{
		reward.stealOldestVoice().play(-18);
		boom.getCurrent().release();
		base.onButtonFinished();
	}
	void onAudioLoad()
	{
		synth = fxSynth.getSynthesizer();

		boom = new SoundPrefabs.SonicBoom(synth);
		boom.initializeWithVoices(2);

		click = new SoundPrefabs.SimpleButtonClick(synth);
		click.initializeWithVoices(2);

		reward = new VoiceBuffer<FMSynth.Voice>(synth);
		reward.initialize(2,
			voice =>
			{
				var op = voice.createAndAddOperator();
				op.envelope.setADSR(1000, 500, -48, 100);
				op.isModulator = true;
				op.volume = 0.1f;

				op = voice.createAndAddOperator();
				op.envelope.setADSR(10, 500, -48, 100);

				voice.setPitch(EnvironmentTuning.A4 + 5, 0);
			}
		);
	}

	void Start()
	{
		GameState.instance().onGameStart += startGame;
		GameState.instance().onAudioIsLoaded += onAudioLoad;
	}
}
