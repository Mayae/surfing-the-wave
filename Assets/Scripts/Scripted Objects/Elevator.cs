﻿/*************************************************************************************

	Surfing The Wave - Interactive Procedural Sound Design Game - v. 0.1.0.

	Copyright (C) 2015 Janus Lynggaard Thorborg [LightBridge Studios]

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.

*************************************************************************************/

using UnityEngine;
using System.Collections;
using Blunt;
using Blunt.Synthesis;
using Blunt.UnityExt;

public class Elevator : MonoBehaviour
{
	public float startPosition = -0.5f;
	public float endPosition = 0.5f;
	public float escalationDelay = 2f;
	public float escalationSpeed = 5f;
	public float shepardSpeed = 4f;
	public float shepardVolume = -18;
	public float rumbleVolume = -9f;
	public float beepVolume = -24f;
	public FXComponentSynth fxInstrument;

	private FMSynth synth;
	private SoundPrefabs.SonicBoom boom;
	private VoiceBuffer<FMSynth.Voice> elevatorBeep;
	private SoundPrefabs.ShepardTone shepardTone;
	private Transform tsf;

	private float enterTime;
	private float lastTimeAtConnection;
	private bool isConnected;
	private bool isEscalating;
	private bool hasReachedTop;
	private bool isTiming;
	private float end, start, deltaT;
	private float elevationProgress;
	private FMSynth.Voice startingBoom;

	
	private void onEscalationStop()
	{
		// release the 'rumble'
		startingBoom.release();
		// start another boom,
		var v = boom.stealOldestVoice();
		v.play(rumbleVolume);
		// and release it in a quarter second.
		this.delayedAction(() => v.release(), 0.25f);

		// release the shepard tone as well
		shepardTone.release();
	}

	private void onEscalationStart()
	{
		elevatorBeep.getCurrent().release();

		startingBoom = boom.stealOldestVoice();
		startingBoom.play(rumbleVolume);

		shepardTone.play(shepardVolume, 11.25f);
		elevationProgress = 0;
	}

	private void onElevatorEnter()
	{
		elevatorBeep.stealOldestVoice().play(beepVolume);
	}

	private void onAudioLoaded()
	{

		synth = fxInstrument.getSynthesizer();
		boom = new SoundPrefabs.SonicBoom(synth);
		shepardTone = new SoundPrefabs.ShepardTone(synth, 6, shepardSpeed);
		shepardTone.initializeWithVoices(2);
		boom.initializeWithVoices(2);
		elevatorBeep = new VoiceBuffer<FMSynth.Voice>(synth);
		elevatorBeep.initialize(2,
			voice =>
			{
				var op = voice.createAndAddOperator();
				op.envelope.setADSR(1000, 500, -48, 100);
				op.isModulator = true;
				op.volume = 0.1f;

				op = voice.createAndAddOperator();
				op.envelope.setADSR(10, 500, -48, 100);

				voice.setPitch(EnvironmentTuning.A4 + 5, 0);
			}
		);

	}


	public void OnCollisionEnter2D(Collision2D c)
	{
		enterTime = Time.time;
	}
	public void OnCollisionStay2D(Collision2D c)
	{
		isConnected = true;
		lastTimeAtConnection = Time.time;
	}
	public void OnCollisionExit2D(Collision2D c)
	{
		isConnected = false;
	}
	// Use this for initialization
	void Start ()
	{
		hasReachedTop = isConnected = isEscalating = false;
		tsf = GetComponent<Transform>();
		GameState.instance().onAudioIsLoaded += onAudioLoaded;
	}


	// Update is called once per frame
	void Update ()
	{
		if(!isEscalating)
		{
			if(!isConnected)
			{
				isTiming = false;
				// this makes the elavator move down, if it reached the top.
				if(hasReachedTop && (Time.time - lastTimeAtConnection) > escalationDelay)
				{
					isEscalating = true;
					onEscalationStart();
				}
				else
				{
					enterTime = Time.time;
				}
			}
			else
			{
				if(!isTiming)
				{
					onElevatorEnter();
					isTiming = true;
				}
				if((Time.time - enterTime) > escalationDelay)
				{
					if(!hasReachedTop)
					{
						isEscalating = true;
						onEscalationStart();
					}

				}
			}
		}
		else
		{

			start = hasReachedTop ? endPosition : startPosition;
			end = !hasReachedTop ? endPosition : startPosition;

			elevationProgress += Time.deltaTime / escalationSpeed;
	
			var progress = Mathf.SmoothStep(0, 1, elevationProgress);

			shepardTone.setProgress(hasReachedTop ? 1 - progress : progress);

			
			tsf.localPosition = new Vector3(tsf.localPosition.x, 
				Mathf.SmoothStep(start, end, elevationProgress));
			bool condition = elevationProgress > 1;

			if(elevationProgress > 1)
			{
				isEscalating = false;
				hasReachedTop = !hasReachedTop;
				enterTime = Time.time;
				onEscalationStop();
			}

		}
	}
}
