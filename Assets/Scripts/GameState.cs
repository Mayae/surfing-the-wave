﻿/*************************************************************************************

	Surfing The Wave - Interactive Procedural Sound Design Game - v. 0.1.0.

	Copyright (C) 2015 Janus Lynggaard Thorborg [LightBridge Studios]

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.

*************************************************************************************/

using UnityEngine;
using System.Collections;
using System;

using SystemFloat = System.Single;


public class GameState : MonoBehaviour
{
	public delegate void GameEventAction();
	
	// Game variables
	public Player mainPlayer;
	/// <summary>
	/// The current player velocity
	/// </summary>
	public Vector3 playerVelocity;
	/// <summary>
	/// The players velocity one fixed update ago.
	/// </summary>
	public Vector3 playerVelocityInLastFixedUpdate;
	public MapGeneration mapGenerator;

	public int numAbilitiesPickedUp
	{
		get
		{
			return numPickups;
		}
	}

	public float gameTime
	{
		get
		{
			if(hasGameBeenLoaded)
			{
				return Time.time - timeAtGameStart;
			}
			else
			{
				return 0;
			}
		}
	}

	public float airResistance
	{
		get
		{
			return Mathf.Clamp01(playerVelocity.magnitude / (mainPlayer.maximumVelocity * 2.5f));
		}
	}

	public float gametimeDrift
	{
		get
		{
			return timeAtGameStart;
		}
	}
	public float soundTime
	{
		get
		{
			return (float)ph.timeInSeconds;
		}
	}
	// Game events
	public GameEventAction onJump;
	public GameEventAction onAirborne;
	public GameEventAction onGrounded;
	public GameEventAction onDeath;
	public GameEventAction onRestart;
	public GameEventAction onPickup;
	public GameEventAction onWaterEntry;
	public GameEventAction onWaterExit;
	public GameEventAction onQuietEntry;
	public GameEventAction onQuietExit;
	public GameEventAction onPuzzleEnter;
	public GameEventAction onPuzzleSolved;
	public enum State
	{
		StartMenu,
		Ingame,
		Highscore
	}

	public State currentGameState = State.StartMenu;

    public float currentTimeScale
    {
        get
        {
            return curTimeScale;
        }
    }

    private float curTimeScale = 1.0f;
	public float pausedTimeScale = 0.0f;
	// Game controls

	/// <summary>
	/// This is called after all possible initialization has been done
	/// Ie. after constructions, field initialization, Awake() and Start()
	/// Will be called on the MAIN thread, after all other initialization has been done.
	/// </summary>
	public GameEventAction onLoad;
	/// <summary>
	/// Will be called AFTER onLoad, and AFTER all initialization, and on the audio thread, and before onGameIsLoaded!.
	/// Here you should allocate all the resources you need yourself.
	/// </summary>
	public GameEventAction onAudioLoad;
	/// <summary>
	/// Called directyl after onAudioLoad. Here you should update the resources you need from other components.
	/// </summary>
	public GameEventAction onAudioIsLoaded;
	/// <summary>
	/// Will be called on game shutdown, on the main thread.
	/// </summary>
	public GameEventAction onExit;
	/// <summary>
	/// This is called when the game displays the loading progress.
	/// </summary>
	public GameEventAction onGameLoad;
	/// <summary>
	/// This is called when everything is fit to go (onLoad and onAudioLoad has been finished).
	/// </summary>
	public GameEventAction onGameIsLoaded;
	/// <summary>
	/// This is called when the main menu is loaded.
	/// </summary>
	public GameEventAction onGameMenu;
	/// <summary>
	/// Called when something pauses the game - but before it is actually paused.
	/// </summary>
	public GameEventAction onGamePaused;
	/// <summary>
	/// Called when something resumes the game - but before the game is actually resumed.
	/// </summary>
	public GameEventAction onGameResumed;
	/// <summary>
	/// This is called when the game starts.
	/// </summary>
	public GameEventAction onGameStart;
	/// <summary>
	/// This is called when the game ends, ie. an ideal place to show highscores, and such.
	/// </summary>
	public GameEventAction onGameEnd;
	/// <summary>
	/// The game logic loop. This is called before all others.
	/// </summary>
	public GameEventAction onUpdate;
	/// <summary>
	/// The game physics / fixed update loop, called before all others.
	/// </summary>
	public GameEventAction onPhysics;
	/// <summary>
	/// This event is called AFTER Unity's internal physics calculations.
	/// </summary>
	public GameEventAction onPostPhysics;
	/// <summary>
	/// This event is called BEFORE Unity's
	/// </summary>
	public GameEventAction onPrePhysics;
	/// <summary>
	/// This function can be used to render some audio for testing purposes.
	/// </summary>
	public event Action<SystemFloat[], int> onMutedAudio;

	// Internals
	static private GameState internalInstance;
	private bool hasAudioBeenLoaded;
	private bool hasInitializersBeenRun;
	private bool gameIsPaused;
	private Blunt.CResizableContainer<SystemFloat> muteBuffer;
	private GameObject initer;
	private Blunt.Synthesis.PlayHead ph;
	private bool hasGameBeenLoaded;
	private float timeAtGameStart = 0.0f;
	private int numPickups;
	private Vector3 newVelocity;
	protected class InitializerEnd : MonoBehaviour
	{
		void Start()
		{
			//signal(internalInstance.onInitializationEnd);
		}
	}



	static public void signal<T>(T e) where T : class
	{

		GameEventAction eventAction = e as GameEventAction;
		if(eventAction != null)
		{
			eventAction();
		}
	}

	static public GameState instance()
	{
		if(internalInstance == null)
		{
			GameObject gameObject = new GameObject("Game State");
			internalInstance = gameObject.AddComponent<GameState>();
		}
		return internalInstance;
	}

	private void onPickupInternal()
	{
		numPickups++;
		if(numAbilitiesPickedUp >= mapGenerator.pickupPositions.Length)
		{
			onGameEnd();
		}
	}

	public void pauseGame()
	{
		if(!gameIsPaused)
		{
			if(onGamePaused != null)
				onGamePaused();
			curTimeScale = Time.timeScale;
			Time.timeScale = pausedTimeScale;
			gameIsPaused = true;
		}
	}

	public void resumeGame()
	{
		if(gameIsPaused)
		{
			if(onGameResumed != null)
				onGameResumed();
			Time.timeScale = curTimeScale;
		}
	}

	void Awake()
	{
		if(internalInstance != null)
		{
			if(!object.ReferenceEquals(this, internalInstance))
			{
				Debug.Log("Error: More than one instance of GameState exists!");
			}
		}
		else
		{
			internalInstance = this;
		}
	}

	private void onGameStartInternal()
	{
		timeAtGameStart = Time.time;
		numPickups = 0;
		currentGameState = State.Ingame;
		gameIsPaused = false;
	}

	public GameState()
	{

		hasAudioBeenLoaded = false;
		hasInitializersBeenRun = false;
		muteBuffer = new Blunt.CResizableContainer<float>();

#if UNITY_EDITOR
		// add debugging:
		onLoad += () => print("Event: onLoad");
		onAudioLoad += () => print("Event: onAudioLoad");
		onAudioIsLoaded += () => print("Event: onAudioIsLoaded");
		onExit += () => print("Event: onExit");
		onGameLoad += () => print("Event: onGameLoad");
		onGameIsLoaded += () => print("Event: onGameIsLoaded");
		onGameMenu += () => print("Event: onGameMenu");
		onGameStart += () => print("Event: onGameStart");
		onGameEnd += () => print("Event: onGameEnd");
		onJump += () => print("Event: onJump");
		onAirborne += () => print("Event: onAirBorne");
		onGrounded += () => print("Event: onGrounded");
		onDeath += () => print("Event: onDeath");
		onRestart += () => print("Event: onRestart");
		onPickup += () => print("Event: onPickup");
		onQuietEntry += () => print("Event: onQuietEntry");
		onQuietExit += () => print("onQuietExit");
#endif

		onJump += () => { if(onAirborne != null) onAirborne(); };
		onLoad += onGameLoad;
		onGameIsLoaded += () =>
		{
			hasGameBeenLoaded = true; 
		};

		onGameStart += onGameStartInternal;
		onGameEnd += () => { currentGameState = State.Highscore; };
		onPickup += onPickupInternal;
		ph = new Blunt.Synthesis.PlayHead();

		print("Game state initialized");

	}
	void Update()
	{
		if(initer == null && !hasInitializersBeenRun)
		{
			initer = new GameObject("Game Initialization Check");
			initer.AddComponent<InitializerEnd>();
			hasInitializersBeenRun = true;
			if(onLoad != null)
				onLoad();
		}
		if(!hasGameBeenLoaded && hasInitializersBeenRun && hasAudioBeenLoaded)
		{
            onGameIsLoaded();
		}

		var rgdb = mainPlayer.rigidbody2D;
		if(rgdb != null)
		{
			playerVelocity = rgdb.velocity;
		}

		if(onUpdate != null && hasGameBeenLoaded)
		{
			onUpdate();
		}
	}
	public void Start()
	{

		// add some event handlers..
		Blunt.Utils.ExtendedPhysics.instance().lateFixedUpdateCallback += postPhysics;
		gameObject.AddComponent<AudioSource>();
	}
	public void FixedUpdate()
	{
		playerVelocityInLastFixedUpdate = newVelocity;
		var rgdb = mainPlayer.rigidbody2D;
		if(rgdb != null)
		{
			newVelocity = rgdb.velocity;
		}
		if(onPrePhysics != null)
			onPrePhysics();
	}
	public void postPhysics()
	{
		if(onPostPhysics != null)
			onPostPhysics();
	}

	public void OnAudioFilterRead(SystemFloat[] data, int nChannels)
	{
		if(hasInitializersBeenRun)
		{
			muteBuffer.ensureSize(data.Length);
			if(!hasAudioBeenLoaded)
			{
				if(onAudioLoad != null)
					onAudioLoad();
				if(onAudioIsLoaded != null)
					onAudioIsLoaded();
				hasAudioBeenLoaded = true;
			}
			if(hasGameBeenLoaded)
			{
				if(onMutedAudio != null)
					onMutedAudio(muteBuffer.getData(), nChannels);
				ph.advance(data.Length / nChannels);
			}

		}
	}
}
