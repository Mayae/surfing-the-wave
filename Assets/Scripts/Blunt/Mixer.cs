/*************************************************************************************

	blunt - a dull C# library - v. 0.1.0.

	Copyright (C) 2015 Janus Lynggaard Thorborg [LightBridge Studios]

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.

	See \licenses\ for additional details on licenses associated with this program.

*************************************************************************************/

#define BLUNT_DEBUG

using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

namespace Blunt
{

	namespace Sound
	{
		using DSPType = System.Single;
		using SystemFloat = System.Single;

		/// <summary>
		/// Mixers are additive (and associative) containers of channels.
		/// Provides an optional mastering stage.
		/// </summary>
		public class Mixer : MonoBehaviour
		{
			public float readingFilterCoeff = 0.99f;
			public bool useThreadedProcessing = false;

			public bool doReads;
			public class MixerChannel : SoundStage
			{
				private string name;
				private Blunt.CQueue<SoundStage> stages 
					= new Blunt.CQueue<SoundStage>();
				private bool enabled;
				public string getName ()
				{
					return name;
				}
				public bool isEnabled()
				{
					return enabled;
				}

				public void setIsEnabled(bool toggle)
				{
					enabled = toggle;
				}
				public MixerChannel(string nameToUse)
				{
					name = nameToUse;
					enabled = false;
				}

				public void addStage(SoundStage s)
				{
					lock(stages)
					{
						stages.Add(s);
					}
				}

				public void removeStage(SoundStage s)
				{
					lock(stages)
					{
						stages.Remove(s);
					}
				}

				public void process (float[] data, int nSampleFrames, int channels, bool channelIsEmpty)
				{
					lock(stages)
					{
						foreach(SoundStage s in stages)
						{
							s.process(data, nSampleFrames, channels, false);
						}
					}
				}
			};

			public class Reading
			{
				/// <summary>
				/// The average squared mean with peaks, as a 0..1 fraction.
				/// </summary>
				public DSPType meanLeftAverage;
				public DSPType meanRightAverage;

				/// <summary>
				/// These are the squared peak-smoothed values. 
				/// </summary>
				public DSPType smoothedSquarePeakLeft;
				public DSPType smoothedSquarePeakRight;

				/// <summary>
				/// The name of the channel.
				/// </summary>
				public string channelName;

				/// <summary>
				/// The amount of frames processed this current callback.
				/// </summary>
				public float sampleFrames;

				/// <summary>
				/// The time in miliseconds it took processing this channel.
				/// </summary>
				public double processingTime;

				public readonly MixerChannel channelReference;

				/// <summary>
				/// Returns the maximum root-mean-square average of
				/// all the channels, with peaks considered, in decibels.
				/// </summary>
				/// <returns>The max reading for channels.</returns>
				public DSPType getMaxDBReadingForChannels()
				{
					var value = System.Math.Sqrt(
						System.Math.Max (smoothedSquarePeakLeft, smoothedSquarePeakRight)
					);

					return (DSPType)Blunt.MathExt.fractionToDB (value);
				}

				public Reading(MixerChannel channel)
				{
					channelName = channel.getName();
					channelReference = channel;
					meanLeftAverage = meanRightAverage 
						= smoothedSquarePeakLeft = smoothedSquarePeakRight = 0;
				}
			}



			public enum ErrorCodes
			{
				None,
				Disabled,
				ThreadingError,
				ExceptionDuringProcessing,
				UnsupportedNumberOfChannels,
				SoundSystemIsLoading

			};

			void Start()
			{
				errorCode = ErrorCodes.Disabled;
				mixerName = "Default mixer name";
				GameState.instance().onGameIsLoaded += () => gameSoundIsInitialized = true;
			}

			public void setIsEnabled(bool toggle)
			{
				isEnabled = toggle;
			}

			public void setName(string newName)
			{
				mixerName = newName;
			}

			public string getName()
			{
				return mixerName;
			}

			public ErrorCodes getCurrentError()
			{
				return errorCode;
			}

			public MixerChannel getChannel(string name)
			{
				lock(channels)
				{
					if(channels.ContainsKey(name))
					{
						return channels[name].channel;
					}
					else
					{
						channels.Add (name, new ChannelEntry(name));
						return channels[name].channel;
					}
				}
			}

			public void removeListenerForChannel(string name, SoundStage s)
			{
				lock(channels)
				{
					if(channels.ContainsKey(name))
					{
						channels[name].listeners.RemoveAll(i => i == s);
					}
				}
			}

			public bool addListenerForChannel(string name, SoundStage s)
			{
				lock(channels)
				{

					if(!channels.ContainsKey(name))
#if !BLUNT_DEBUG
						return false;
#else
					{
						throw new ArgumentException("No such mixer channel: " + name);

					}
#endif
					channels[name].listeners.Add (s);
					return true;
				}
			}
			
			public void removeChannel(string name)
			{
				lock(channels)
				{
					if(channels[name] != null)
					{
						channels.Remove(name);
					}
				}
			}


			public void removeChannel(MixerChannel channel)
			{
				lock(channels)
				{
					foreach(var it in channels)
					{
						if(it.Value.channel == channel)
						{
							channels.Remove(it.Key);
							// keys are unique: can only have one, so return already.
							return;
						}
					}
				}
			}

			/// <summary>
			/// Gets the readings.
			/// </summary>
			/// <returns>The readings.</returns>
			public List<Reading> getReadings()
			{
				List<Reading> list = new List<Reading> ();
				foreach(var it in channels)
				{
					list.Add(it.Value.reading);
				}

				return list;
			}
			/// <summary>
			/// Enables post measurements of all channels, giving pollable info
			/// like RMS levels and such.
			/// </summary>
			/// <param name="toggle">If set to <c>true</c> toggle.</param>
			public void enableMixerMeasurements(bool toggle)
			{
				doReads = toggle;
			}


			public void OnAudioFilterRead(SystemFloat[] data, int nChannels)
			{
				if(!gameSoundIsInitialized)
				{
					errorCode = ErrorCodes.SoundSystemIsLoading;
					return;
				}
				// early returns.
				if(!isEnabled)
				{
					errorCode = ErrorCodes.Disabled;
					return;
				}

				if(nChannels != 2)
				{
					errorCode = ErrorCodes.UnsupportedNumberOfChannels;
					return;
				}

				if(useThreadedProcessing)
				{
					// Need NET 4.5 (Threading.Tasks.Parallel.For
					errorCode = ErrorCodes.ThreadingError;
					return;
				}

				errorCode = ErrorCodes.None;
				buffer.ensureSize (data.Length);

				lock (channels) 
				{
					double timeForThisChannel = 0.0;
					ChannelEntry cur;
					foreach(var it in channels)
					{
						cur = it.Value;
						if(doReads)
						{
							watch.Reset();
							watch.Start ();
						}
						// zero out buffer.
						var output = buffer.getData();
						System.Array.Clear (output, 0, data.Length);
						// process this channel.
						cur.channel.process(output, data.Length / nChannels, nChannels, true);

						// run over the buffer with an averaging filter, 
						// absorbing any peaks.
						if(doReads)
						{
							timeForThisChannel = watch.Elapsed.TotalMilliseconds;

							var currentLeft = cur.reading.smoothedSquarePeakLeft;
							var currentRight = cur.reading.smoothedSquarePeakRight;
							SystemFloat left, right, squaredLeft, squaredRight;

							// notice we only iterate over data.Length!!
							// output buffer may be larger.
							for(int s = 0; s < data.Length; s += nChannels)
							{
								left = output[s];
								right = output[s + 1];

								squaredLeft = left * left;
								squaredRight = right * right;


								// Note: Candidate for SIMD branchless computation.
								if(squaredLeft > currentLeft)
								{
									currentLeft = squaredLeft;
								}
								else
								{
									currentLeft *= readingFilterCoeff;
								}

								if(squaredRight > currentRight)
								{
									currentRight = squaredRight;
								}
								else
								{
									currentRight *= readingFilterCoeff;
								}

								/*	For using a RMS IIR filter:
								currentLeft = squaredLeft * readingFilterCoeff * (currentLeft - squaredLeft);
								currentRight = squaredRight * readingFilterCoeff * (currentRight - squaredRight);
								*/

								// accumulate output buffers.
								data[s] += left;
								data[s + 1] += right;
							}
							cur.reading.smoothedSquarePeakLeft = currentLeft;
							cur.reading.smoothedSquarePeakRight = currentRight;
							cur.reading.processingTime = timeForThisChannel;
							cur.reading.sampleFrames = data.Length / nChannels;
						}
						else // only copy over data.
						{
							// Note: Candidate for bitwise copy / unrolling?.
							for(int s = 0; s < data.Length; s++)
							{
								data[s] += output[s];
							}
						}
						// call channel listeners
						foreach(var list in cur.listeners)
						{
							list.process(output, data.Length / nChannels, nChannels, true);
						}
					}
					/*for(int s = 0; s < data.Length; s++)
					{
						data[s] = 1;
					}*/

				}

			}

			private class ChannelEntry
			{
				public MixerChannel channel;
				public readonly string name;
				public List<SoundStage> listeners;
				public Reading reading;
				public ChannelEntry(string nameToUse)
				{
					name = nameToUse;
					channel = new MixerChannel(name);
					listeners = new List<SoundStage>();
					reading = new Reading(channel);
				}
			};

			private Dictionary<string, ChannelEntry> channels = new Dictionary<string, ChannelEntry>();
			private CResizableContainer<SystemFloat> buffer = new CResizableContainer<SystemFloat>();
			private bool isEnabled = false;
			private bool gameSoundIsInitialized = false;
			private ErrorCodes errorCode = ErrorCodes.Disabled;
			private string mixerName;
			private System.Diagnostics.Stopwatch watch
				= new System.Diagnostics.Stopwatch();

		};

	}
}