/*************************************************************************************

	blunt - a dull C# library - v. 0.1.0.

	Copyright (C) 2015 Janus Lynggaard Thorborg [LightBridge Studios]

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.

	See \licenses\ for additional details on licenses associated with this program.

**************************************************************************************

	file:CHighScore.cs

		Master file.

*************************************************************************************/


using System.Collections;
using UnityEngine;
using System;
using System.Collections.Generic;

namespace Blunt
{
    /*		
	 * Class that maintains highscores composed of integer scores and an
	 * associated string name.
	 */
    class CHighScore
    {

        static int maxScoresToKeepTrackOf = 5;

        public class PlayerEntry : System.IComparable
        {
            public string playerName = "";
            public int score;
            public PlayerEntry(string name, int highscore)
            {
                playerName = name;
                score = highscore;
            }

            public PlayerEntry()
            {
                playerName = "";
                score = -1;
            }

            public int CompareTo(object obj)
            {
                if (obj == null) return 1;

                PlayerEntry other = obj as PlayerEntry;
                if (other != null)
                    return this.score.CompareTo(other.score);
                else
                    throw new System.ArgumentException("Object is not a PlayerEntry");
            }
        };

        static public void submitScore(PlayerEntry entry)
        {
            PlayerEntry[] scores = getScores();
            PlayerEntry[] newScores = new PlayerEntry[scores.Length + 1];
            for (int i = 0; i < scores.Length; ++i)
            {
                newScores[i] = new PlayerEntry();
                newScores[i].playerName = scores[i].playerName;
                newScores[i].score = scores[i].score;

            }
            newScores[scores.Length] = new PlayerEntry();
            newScores[scores.Length].playerName = entry.playerName;
            newScores[scores.Length].score = entry.score;
            System.Array.Sort(newScores);
            System.Array.Reverse(newScores);
            writeScores(newScores);
        }

        static public PlayerEntry[] getScores()
        {
            PlayerEntry[] entries = new PlayerEntry[maxScoresToKeepTrackOf];
            for (int i = 0; i < maxScoresToKeepTrackOf; ++i)
            {
                entries[i] = new PlayerEntry();
                entries[i].playerName = UnityEngine.PlayerPrefs.GetString("Playername" + i.ToString());
                entries[i].score = UnityEngine.PlayerPrefs.GetInt("Playerscore" + i.ToString());
            }
            return entries;
        }
        static private void writeScores(PlayerEntry[] entries)
        {
            for (int i = 0; i < maxScoresToKeepTrackOf || i < entries.Length; ++i)
            {
                UnityEngine.PlayerPrefs.SetString("Playername" + i.ToString(), entries[i].playerName);
                UnityEngine.PlayerPrefs.SetInt("Playerscore" + i.ToString(), entries[i].score);
            }
            UnityEngine.PlayerPrefs.Save();
        }

        static public void wipeHighscore()
        {
            for (int i = 0; i < maxScoresToKeepTrackOf; ++i)
            {
                UnityEngine.PlayerPrefs.SetString("Playername" + i.ToString(), "");
                UnityEngine.PlayerPrefs.SetInt("Playerscore" + i.ToString(), 0);
            }
            UnityEngine.PlayerPrefs.Save();
        }

    };

};
