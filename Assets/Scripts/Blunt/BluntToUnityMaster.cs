﻿/*************************************************************************************

	blunt - a dull C# library - v. 0.1.0.

	Copyright (C) 2015 Janus Lynggaard Thorborg [LightBridge Studios]

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.

	See \licenses\ for additional details on licenses associated with this program.

*************************************************************************************/

using UnityEngine;
using System.Collections;
/// <summary>
/// Master audio source - adding this component after other components
/// enables Unity Mixing on previous components.
/// </summary>
public class BluntToUnityMaster : MonoBehaviour
{
	public Transform positionIn3DSpace;
	// Use this for initialization
	void Start ()
    {
		if(positionIn3DSpace != null)
		{
			GetComponent<Transform>().parent = positionIn3DSpace;
		}
	}
	public void OnAudioFilterRead(float[] data, int nChannels)
	{

	}
	// Update is called once per frame
	void Update ()
    {
	
	}
}
