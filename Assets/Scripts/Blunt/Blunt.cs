/*************************************************************************************

	blunt - a dull C# library - v. 0.1.0.

	Copyright (C) 2015 Janus Lynggaard Thorborg [LightBridge Studios]

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.

	See \licenses\ for additional details on licenses associated with this program.

*************************************************************************************/
//#if UNITY_EDITOR
	#define HAS_MONO
///#endif

using System.Collections;
using UnityEngine;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;

#if HAS_MONO
using Mono.Simd;
#endif
namespace Blunt
{
	/*		
	 * Generic O(1) queue that remembers the first element.
	 */
	public class CFrontQueue<T> : Queue<T>
	{
		
		public T newest() {
			return newestElement;
		} 
		
		public void push(T element)
		{
			Enqueue (newestElement = element);
		}
		
		public T pop() {
			return Dequeue ();
		}
		
		public void clear()
		{
			newestElement = default(T);
			Clear ();
		}
		
		private T newestElement = default(T);
	};

	public class CResizableContainer<T>  
	{
		private T[] data;

		public T[] getData() 
		{
			return data;
		}

		public void clearRange(int index, int elements)
		{
			System.Array.Clear (data, index, elements);
		}

		public void ensureSize(long size)
		{
			if(size > 0 && data == null || size > data.Length)
			{
				data = new T[size];
			}
		}

	}

	/*		
	 * Generic queue O(n) for references
	 */
	public class CQueue<T> : List<T> where T : class
	{
		
		public T back() 
		{
			return newestElement;
		} 
		// peeking
		public T front() 
		{
			return base.Count > 0 ? base [0] : null;
		}
		public void push(T element)
		{
			base.Add(newestElement = element);
		}

		public void remove(T element)
		{
			if(base.Count > 0)
			{
				if(element == newestElement)
				{
					base.Remove(element);
					if(base.Count > 1)
					{
						newestElement = base[base.Count - 1];
					}
					else if(base.Count == 1)
					{
						newestElement = base[0];
					}
					else
					{
						newestElement = null;
					}

				}
				else
				{
					base.Remove(element);
				}
			}
		}
		
		public T pop() 
		{
			if(base.Count > 0)
			{
				T obj = front();
				base.RemoveAt(0);
				return obj;
			}
			return null;
		}
		
		public void clear()
		{
			newestElement = default(T);
			Clear ();
		}
		
		private T newestElement = default(T);
	};



	class Utils
	{

		/// <summary>
		/// Thread-safe initial highly random device. Usefull for initializing seeds on other devices.
		/// </summary>
		public class SafeSystemRandom
		{
			private static System.Random r = new System.Random();
			/// <summary>
			/// 0 .. 1 
			/// </summary>
			/// <returns></returns>
			public static float getNextFloat()
			{
				return (float)getNextDouble();
			}
			/// <summary>
			/// 0 .. 1
			/// </summary>
			/// <returns></returns>
			public static double getNextDouble()
			{
				lock (r)
				{
					return r.NextDouble();
				}
			}
			/// <summary>
			/// int.min .. int.max
			/// </summary>
			/// <returns></returns>
			public static int getNextInteger()
			{
				lock(r)
				{
					return r.Next(int.MinValue, int.MaxValue);
				}
			}
		}
		/// <summary>
		/// This struct is _not_ threadsafe, however it is extremely cheap and lightweight (4 byte state)
		/// </summary>
		public class CheapRandom
		{
			int seed;

			public CheapRandom(int s = 0)
			{
				if(s == 0)
				{
					// seed cannot be zero.
					while((seed = SafeSystemRandom.getNextInteger()) == 0)
						;
				}
				else
					seed = s;
				//seed = 1;
			}
			/// <summary>
			/// returns the next random float between 0 and 1
			/// </summary>
			/// <returns></returns>
			public float random01()
			{
				unchecked
				{
					seed *= 16807;
					return (float)(seed & 0x7FFFFFFF) * 4.6566129e-010f;
				}

			}
			/// <summary>
			/// returns the next random float between -1 and 1
			/// </summary>
			/// <returns></returns>
			public float random11()
			{
				unchecked
				{
					seed *= 16807;
					return (float)seed * 4.6566129e-010f;
				}

			}

			public int randomRange(int min, int max)
			{
				unchecked
				{
					float value = random01();
					return Mathf.RoundToInt(min + max * value);
				}
			}
			/// <summary>
			/// Expects the chances to sum to 1.
			/// </summary>
			/// <param name="chances"></param>
			/// <returns></returns>
			public int chooseFromChances(float[] chances)
			{
#if CUMULATIVE_RANDOM
				var value = random01();
				float sum = 0;
				for(int i = 0; i < chances.Length; ++i)
				{
					sum += chances[i];
					if(sum > value)
						return i;
				}

#else
				var value = random01();
				for(int i = 0; i < chances.Length; ++i)
				{
					if(value < chances[i])
						return i;
					value -= chances[i];
				}
#endif
				return 0;

			}
			/// <summary>
			/// Will normalize the weights before choosing a alue.
			/// </summary>
			/// <param name="chances"></param>
			/// <returns></returns>
			public int chooseFromChancesUnnormalized(float[] chances)
			{
				// get the normalized sum:
				float sum = 0f;
				for(int i = 0; i < chances.Length; ++i)
				{
					sum += chances[i];
				}
#if CUMULATIVE_RANDOM
				var value = random01() * sum;
				float sum = 0;
				for(int i = 0; i < chances.Length; ++i)
				{
					sum += chances[i];
					if(sum > value)
						return i;
				}

#else

				var value = random01() * sum;
				for(int i = 0; i < chances.Length; ++i)
				{
					if(value < chances[i])
						return i;
					value -= chances[i];
				}
#endif
				return 0;

			}

		}



		public class Once
		{
			private bool instantiated;
			System.Action action;
			public Once(System.Action a)
			{
				action = a;
			}
			
			public void fire()
			{
				if(!instantiated)
				{
					instantiated = true;
					action();
				}
			}
			
			void reset()
			{
				instantiated = false;
			}
		};

		public class ExtendedPhysics : MonoBehaviour
		{
			
			static public Vector3 offscreenColliderPosition = new Vector3(-100, -100, -100);
			public event System.Action lateFixedUpdateCallback;
			
			static private ExtendedPhysics internalInstance;
			private bool fixedUpdateExecuted = false;
			
			static public ExtendedPhysics instance()
			{
				if(!internalInstance)
				{
					// create a 'fake' new ExtendedPhysicsBehaviour
					var proxy = new GameObject("PhysicsProxyInstance");
					// add ourselves
					internalInstance = proxy.AddComponent<ExtendedPhysics>();
					//internalInstance.GetComponent<Transform>().position = offscreenColliderPosition;
					internalInstance.gameObject.AddComponent<BoxCollider>().isTrigger = true;

					// create a colliding game object.
					var obj = new GameObject("PhysicsProxyCollider");
					// add colliding objects, causing OnCollisionStay events to be fired
					// AFTER all physics calculations.
					obj.AddComponent<BoxCollider>();
					obj.AddComponent<Rigidbody>();
					obj.rigidbody.isKinematic = true;
					obj.GetComponent<Transform>().parent = internalInstance.GetComponent<Transform>().transform;
					obj.GetComponent<Transform>().position = internalInstance.GetComponent<Transform>().position;
					
				}
				return internalInstance;
			}
			
			void FixedUpdate() 
			{
				// signal that fixed update has happened
				fixedUpdateExecuted = true;
			}
			
			void OnTriggerStay() 
			{
				//print ("Triggering");
				if(fixedUpdateExecuted)
				{
					fixedUpdateExecuted = false;
					if (lateFixedUpdateCallback != null)
					{
						lateFixedUpdateCallback();
					}
				}
			}
		}

	};



#if HAS_MONO
		public class SimdExt
		{
			// some of these are already in Vector4f, however
			// the get/set system sometimes crashes during compilation
			static public Vector4f vSignMask = composeFrom(~0x80000000);
			static public Vector4f vSignBit = composeFrom(0x80000000);
			static public Vector4f vExponentMask = composeFrom(0x7FE00000);
			static public Vector4f vAllBits = composeFrom(~0);
			static public Vector4f vNoBits = composeFrom(0);
			static public Vector4f vPiTwo = new Vector4f((float)(Math.PI * 2));
			static public Vector4f vPiRec = new Vector4f((float)(1.0 / Math.PI));
			static public Vector4f vPiTwoRec = new Vector4f((float)(1.0 / (Math.PI * 2)));
			static public Vector4f vOne = new Vector4f(1);
			static public Vector4f vPi = new Vector4f((float)Math.PI);
			static public Vector4f vLN2 = new Vector4f((float)Math.Log(2));
			static public Vector4f vLN2Rec = new Vector4f((float)(1.0 / Math.Log(2)));
			static public Vector4f composeFrom(uint input)
			{
				return new Vector4f(BitConverter.ToSingle(BitConverter.GetBytes(input), 0));
			}

			static public Vector4f composeFrom(int input)
			{
				return new Vector4f(BitConverter.ToSingle(BitConverter.GetBytes(input), 0));
			}

			public Vector4f bitNot(Vector4f input)
			{
				return VectorOperations.AndNot(input, vAllBits);
			}

		}
#endif
	public class MathExt
	{
		static public float fPiTwoRec = (float)(1.0 / (Math.PI * 2));
		static public float fPiTwo = (float)(Math.PI * 2);
		static public float fPiHalf = (float)(Math.PI * 0.5);
		static public float fLN2 = (float)(Math.Log(2));
		/// <summary>
		/// Returns |a| > |b| ? a : b
		/// </summary>
		/// <returns>The max.</returns>
		/// <param name="a">The alpha component.</param>
		/// <param name="b">The blue component.</param>
		public static float AbsMax(float a, float b)
		{
			return (a * a) > (b * b) ? a : b;
		}

		/// <summary>
		/// Returns |a| > |b| ? |a| : |b|
		/// </summary>
		/// <returns>The absolute max.</returns>
		/// <param name="a">The alpha component.</param>
		/// <param name="b">The blue component.</param>
		public static float AbsMaxAbs(float a, float b)
		{
			a *= a;
			b *= b;
			return (a) > (b) ? a : b;
		}

		/// <summary>
		/// Returns |a| > |b| ? |a| : |b|
		/// </summary>
		/// <returns>The absolute max.</returns>
		/// <param name="a">The alpha component.</param>
		/// <param name="b">The blue component.</param>
		public static double AbsMaxAbs(double a, double b)
		{
			a *= a;
			b *= b;
			return (a) > (b) ? a : b;
		}

		/// <summary>
		/// Returns |a| > |b| ? a : b
		/// </summary>
		/// <returns>The max.</returns>
		/// <param name="a">The alpha component.</param>
		/// <param name="b">The blue component.</param>
		public static double AbsMax(double a, double b)
		{
			return (a * a) > (b * b) ? a : b;
		}

		public static float fractionToDB(float val)
		{
			return 20.0f * Mathf.Log10(val);
		}
		
		public static float dbToFraction(float dBs)
		{
			return Mathf.Pow(10.0f, dBs / 20.0f);
		}
		
		public static double fractionToDB(double val)
		{
			return 20.0 * Math.Log10(val);
		}
		
		public static double dbToFraction(double dBs)
		{
			return Math.Pow(10.0, dBs / 20.0);
		}

		public static double confineTo(double val, double min, double max)
		{
			return System.Math.Max (System.Math.Min (val, max), min); 
		}

		public static int confineTo(int val, int min, int max)
		{
			return System.Math.Max (System.Math.Min (val, max), min); 
		}

		public class UnityScale
		{

			public static float exp(float y, float _min, float _max)
			{
				return _min * Mathf.Pow(_max / _min, y);
			}
			public static double exp(double y, double _min, double _max)
			{
				return _min * Math.Pow(_max / _min, y);
			}
			public class Inv
			{

				public static float exp(float y, float _min, float _max)
				{
					return (float)(Math.Log10(y / _min) / Math.Log10(_max / _min));
				}

				public static float linear(float y, float _min, float _max)
				{
					return (y - _min) / (_max - _min);
				}

				public static double exp(double y, double _min, double _max)
				{
					return (double)(Math.Log10(y / _min) / Math.Log10(_max / _min));
				}
				
				public static double linear(double y, double _min, double _max)
				{
					return (y - _min) / (_max - _min);
				}

			}; // Inv
		}
	};
};
