﻿/*************************************************************************************

	blunt - a dull C# library - v. 0.1.0.

	Copyright (C) 2015 Janus Lynggaard Thorborg [LightBridge Studios]

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.

	See \licenses\ for additional details on licenses associated with this program.

*************************************************************************************/

#define KILL_DENORMALS

using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using Mono.Simd;
using DSPType = System.Single;
using v4sf = Mono.Simd.Vector4f;

namespace Blunt
{
	namespace Synthesis
	{

		public class ADSREnvelope
		{


			public enum Stage
			{
				Attack,
				Decay,
				Release
			};
			public bool shouldMoveToRelease;
			public bool didMoveToRelease;
			public int attackTime;
			public DSPType reciprocalAttack;
			public DSPType decayPole, releasePole, attackPole;
			public DSPType sustainLevel;   // the level of sustain the decay approaces from the attackk
			public DSPType auxVol;   // used by decay and release stages for storing current volume 
			public DSPType denormalFlush = MathExt.dbToFraction(-96);
			/// <summary>
			/// For VCA-style exponential-rising attacks, this is where they start.
			/// </summary>
			public DSPType attackStart = MathExt.dbToFraction(-64);
			public DSPType vcaAttack;
			public int deltaSamples;
			// setting this to true generates VCA exponential attack if
			// process VCA is called.
			public bool isVCA = false;
			/// <summary>
			/// Causes the envelope to be generated at lower rates.
			/// </summary>
			public int downSamplingRate = 1;
			public Stage stage;
			/// <summary>
			/// attack is a linear ramp in ms, while the others are how many miliseconds it takes to alter 24 dBs in either range.
			/// </summary>
			/// <param name="attackInMs"></param>
			/// <param name="decayInMs"></param>
			/// <param name="sustainLevelInDBs"></param>
			/// <param name="releaseInMs"></param>
			public virtual void setADSR(DSPType attackInMs, DSPType decayInMs, DSPType sustainLevelInDBs, DSPType releaseInMs)
			{
				if(attackInMs < EnvironmentTuning.deltaT * 1000)
					attackInMs = EnvironmentTuning.deltaT * 1000;
				double db24 = MathExt.dbToFraction(-24);
				double attackStart = MathExt.dbToFraction(-108);
				double decayInFraction = 1.0 / (downSamplingRate * decayInMs * 0.001 * EnvironmentTuning.sampleRate);
				double releaseInFraction = 1.0 / (downSamplingRate * releaseInMs * 0.001 * EnvironmentTuning.sampleRate);
				double attackInFraction = 1.0 / (downSamplingRate * attackInMs * 0.001 * EnvironmentTuning.sampleRate);
				DSPType decay = (DSPType)Math.Pow(db24, decayInFraction);
				DSPType release = (DSPType)Math.Pow(db24, releaseInFraction);
				attackTime = (int)Math.Round(attackInMs * 0.001 * EnvironmentTuning.sampleRate / downSamplingRate);

				attackPole = (DSPType)Math.Pow(1.0 / attackStart, attackInFraction);
				// this will make sure the attack stage is reached inside a full cycle of sse computations
				//attackTime &= 0x3;
				// -- and it must be 4 at least, to ensure we dont get NaNs in the next step!
				attackTime -= attackTime & 0x3;
				if(attackTime < 1)
					attackTime = 4;
				reciprocalAttack = (DSPType)1.0 / attackTime;
				decayPole = decay;
				releasePole = release;
				sustainLevel = MathExt.dbToFraction(sustainLevelInDBs);

				stop();
			}


			public ADSREnvelope()
			{
				auxVol = 1.0f;
				vcaAttack = attackStart;
				shouldMoveToRelease = didMoveToRelease = false;
			}

			public void release()
			{
				shouldMoveToRelease = true;
				didMoveToRelease = false;
			}

			public virtual void stop()
			{
				if(!isVCA)
					auxVol = 1 - sustainLevel;
				else
					auxVol = attackStart;
				stage = Stage.Attack;
				vcaAttack = attackStart;
				deltaSamples = 0;
				shouldMoveToRelease = false;
				didMoveToRelease = false;
				auxVol = 1.0f;
			}
			/// <summary>
			/// For this algorithm, all stages are exponential.
			/// </summary>
			/// <param name="buffer"></param>
			/// <param name="numSamples"></param>
			public void processVCA(DSPType[] buffer, int numSamples)
			{

				// as the decay stage approaches the sustain, it adds the filter to the sustain
				// instead. When we move to release, we add the sustain,
				// because release does an indefinite decay on the total level.
				if(shouldMoveToRelease && !didMoveToRelease)
				{
					didMoveToRelease = true;
					if(stage == Stage.Decay)
						auxVol += sustainLevel;
					//else
					//	auxVol = deltaSamples * reciprocalAttack;
					stage = Stage.Release;
				}

				int rate = downSamplingRate;
				DSPType currentVolume = 0;
				int j = 0;
				do
				{
					switch(stage)
					{
						case Stage.Attack:
						{
							currentVolume = auxVol;
							// special case; this also needs to handle following NaNs

							for(; j < numSamples; j += rate)
							{
								if(currentVolume > 1.0f)
								{
									stage = Stage.Decay;
									// we didn't actually process this sample!
									currentVolume = 1.0f - sustainLevel;
									auxVol = 1.0f - sustainLevel;
									break;
								}
								buffer[j] = currentVolume;
								currentVolume *= attackPole;

							}

							// save progress
							auxVol = currentVolume;
							break;
						}
						case Stage.Decay:
						{
							// And decays.
							currentVolume = auxVol;
							DSPType pole = decayPole;
							DSPType sustain = sustainLevel;
							for(; j < numSamples; j += rate)
							{
								currentVolume *= pole;
								buffer[j] = (currentVolume + sustain);

							}
							// save progress & kill denormals (flushing to zero)
							if(denormalFlush > currentVolume)
								currentVolume = 0;
							auxVol = currentVolume;

							// set currentVolume to something sensible, to avoid being cut:
							currentVolume += sustain;
							break;
						}
						case Stage.Release:
						{
							// And decays.
							currentVolume = auxVol;
							DSPType pole = releasePole;
							for(; j < numSamples; j += rate)
							{
								currentVolume *= pole;
								buffer[j] = currentVolume;

							}
							// save progress & kill denormals
							if(denormalFlush > currentVolume)
							{
								// here it will also be possible to add
								// looping code modes for the operator - ie. you can reset
								// the envelope when it ends here.
								currentVolume = 0;
							}
							auxVol = currentVolume;
							break;
						}
						default:
						{
							// this should REALLY not happen, but we don't wanna hang the audio thread!
							// we end up here if memory somehow got corrupted.
							j = numSamples;
							break;
						}// envelope switch
					}
				} while(j < numSamples); // envelope loop
			}
			/// <summary>
			/// Simd accelerated.
			/// </summary>
			/// <param name="buffer"></param>
			/// <param name="numSamples"></param>
			public void processBatch(DSPType[] buffer, int numSamples)
			{
				const int vectorSize = 4;

				// as the decay stage approaches the sustain, it adds the filter to the sustain
				// instead. When we move to release, we add the sustain,
				// because release does an indefinite decay on the total level.
				if(shouldMoveToRelease && !didMoveToRelease)
				{
					didMoveToRelease = true;
					if(stage == Stage.Decay)
						auxVol += sustainLevel;
					//else
					//	auxVol = deltaSamples * reciprocalAttack;
					stage = Stage.Release;
				}


				DSPType initialAttack = deltaSamples * reciprocalAttack;
				v4sf vattackStop = new v4sf(stage != Stage.Attack ? initialAttack - reciprocalAttack : 1.0f); 

				v4sf vAttack = new v4sf(
					initialAttack, 
					initialAttack + reciprocalAttack, 
					initialAttack + reciprocalAttack * 2, 
					initialAttack + reciprocalAttack * 3
				);
				v4sf vRecipAttack = new v4sf(reciprocalAttack * 4);
				v4sf vAttackStageMask = v4sf.Zero, vResult = v4sf.Zero;

				float filterCoeff = stage == Stage.Release ? releasePole : decayPole;
                v4sf vSustainOffset = new v4sf(stage == Stage.Release ? 0.0f : sustainLevel);
				v4sf vFilterCoeff = new v4sf(filterCoeff * filterCoeff * filterCoeff * filterCoeff);

				v4sf vCoefficient;
				float projectedAttack = auxVol;
				if(stage == ADSREnvelope.Stage.Attack && initialAttack + numSamples * reciprocalAttack > 1.0f)
				{
					projectedAttack = 1.0f;
				}

				float startingVolume = stage == ADSREnvelope.Stage.Release ? projectedAttack : projectedAttack - sustainLevel;
				v4sf vVolume = new v4sf(startingVolume, startingVolume * filterCoeff,
					startingVolume * filterCoeff * filterCoeff, startingVolume * filterCoeff * filterCoeff * filterCoeff);

				v4sf vDenormalFlush = new v4sf(EnvironmentTuning.denormalFlush);

				for(int i = 0; i < numSamples; i += vectorSize)
				{
					// is attack still below the goal? if, mask out release and decay.
					vAttackStageMask = VectorOperations.CompareNotLessEqual(vattackStop, vAttack);
					// select attack or release/decay
					vResult = vAttack & vAttackStageMask | VectorOperations.AndNot(vAttackStageMask, vVolume + vSustainOffset);
					// store result
					ArrayExtensions.SetVector(buffer, vResult, i);
					// this ensures the coefficient is one (thus, not decreasing the currentvolume)
					// as long as attack has not reached the goal.
					vCoefficient = vAttackStageMask & v4sf.One | VectorOperations.AndNot(vAttackStageMask, vFilterCoeff);
					// increase attack.
					vAttack += vRecipAttack;
					// decay / release:
					vVolume = vVolume * vCoefficient;
#if KILL_DENORMALS
					vVolume = vVolume & VectorOperations.CompareLessThan(vDenormalFlush, vVolume);
#endif
				}
				// if attack mask is set, we can move on to decay. 
				// note, this is automatically selected in the loop.
				vAttackStageMask = VectorOperations.AndNot(vAttackStageMask, v4sf.One);
				if((vAttackStageMask.W) > 0.0f && stage == Stage.Attack)
				{
					stage = Stage.Decay;
				}
                auxVol = vResult.W;

				deltaSamples += numSamples;
				deltaSamples = deltaSamples > attackTime ? attackTime : deltaSamples;
				return;

			}
			/// <summary>
			/// Non-simd accelerated version.
			/// </summary>
			/// <param name="buffer"></param>
			/// <param name="numSamples"></param>
			public void processBatch2(DSPType[] buffer, int numSamples)
			{

				// as the decay stage approaches the sustain, it adds the filter to the sustain
				// instead. When we move to release, we add the sustain,
				// because release does an indefinite decay on the total level.
				if(shouldMoveToRelease && !didMoveToRelease)
				{
					didMoveToRelease = true;
					if(stage == Stage.Decay)
						auxVol += sustainLevel;
					//else
					//	auxVol = deltaSamples * reciprocalAttack;
					stage = Stage.Release;
				}

				int dsamples = deltaSamples;
				int rate = downSamplingRate;
				DSPType currentVolume = 0;
				int j = 0;
				do
				{
					switch(stage)
					{
						case Stage.Attack:
							{
								DSPType attack = attackTime;
								// special case; this also needs to handle following NaNs
								if(attack != 0)
								{
									DSPType rattack = reciprocalAttack;
									currentVolume = deltaSamples * reciprocalAttack;
									for(; j < numSamples; j += rate)
									{
										buffer[j] = currentVolume;
										dsamples++;

										if(dsamples >= attack)
										{
											dsamples = 0;
											stage = Stage.Decay;
											// we did actually process this sample.
											j += rate;
											currentVolume -= sustainLevel;
											break;
										}
										currentVolume += rattack;
									}

								}
								else
								{
									dsamples = 0;
									stage = Stage.Decay;
									// we reached maximum volume immediately
									currentVolume = 1;
								}
								// save progress
								auxVol = currentVolume;
								deltaSamples = dsamples;
								break;
							}
						case Stage.Decay:
							{
								// And decays.
								currentVolume = auxVol;
								DSPType pole = decayPole;
								DSPType sustain = sustainLevel;
								for(; j < numSamples; j += rate)
								{
									currentVolume *= pole;
									buffer[j] = (currentVolume + sustain);

								}
								// save progress & kill denormals (flushing to zero)
								if(denormalFlush > currentVolume)
									currentVolume = 0;
								auxVol = currentVolume;

								// set currentVolume to something sensible, to avoid being cut:
								currentVolume += sustain;
								break;
							}
						case Stage.Release:
							{
								// And decays.
								currentVolume = auxVol;
								DSPType pole = releasePole;
								for(; j < numSamples; j += rate)
								{
									currentVolume *= pole;
									buffer[j] = currentVolume;

								}
								// save progress & kill denormals
								if(denormalFlush > currentVolume)
								{
									// here it will also be possible to add
									// looping code modes for the operator - ie. you can reset
									// the envelope when it ends here.
									currentVolume = 0;
								}
								auxVol = currentVolume;
								break;
							}
						default:
							{
								// this should REALLY not happen, but we don't wanna hang the audio thread!
								// we end up here if memory somehow got corrupted.
								j = numSamples;
								break;
							}// envelope switch
					}
				} while(j < numSamples); // envelope loop



			}
		};
	}

}