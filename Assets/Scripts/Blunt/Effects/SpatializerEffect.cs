﻿/*************************************************************************************

	blunt - a dull C# library - v. 0.1.0.

	Copyright (C) 2015 Janus Lynggaard Thorborg [LightBridge Studios]

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.

	See \licenses\ for additional details on licenses associated with this program.

*************************************************************************************/

using UnityEngine;
using System.Collections;
using Mono.Simd;

using v4sf = Mono.Simd.Vector4f;

public class Spatializer : MonoBehaviour
{
	public float rolloffCurve = 1.0f;
	public float panning = 1.0f;
	public Transform listener;
	public Transform tsf;

	public float volume = 1.0f, lpan = 1.0f, rpan = 1.0f;

	// Use this for initialization
	void Start ()
	{
		tsf = GetComponent<Transform>();
	}
	
	// Update is called once per frame
	void Update ()
	{
		var dist = listener.position - tsf.position;
		var sign = dist.x > 0;
        volume = Mathf.Exp(-(listener.position - tsf.position).magnitude / rolloffCurve);
	}

	void OnAudioFilterRead(float[] data, int nChannels)
	{
		float finalLeft = volume * lpan, finalRight = volume * rpan;

		for(int i = 0; i < data.Length; i += nChannels)
		{
			data[i] *= finalLeft;
			data[i + 1] *= finalRight;
		}
	}

}
