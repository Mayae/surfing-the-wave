﻿/*************************************************************************************

	blunt - a dull C# library - v. 0.1.0.

	Copyright (C) 2015 Janus Lynggaard Thorborg [LightBridge Studios]

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.

	See \licenses\ for additional details on licenses associated with this program.

*************************************************************************************/

using UnityEngine;
using System.Collections;
using Blunt;
using Blunt.Sound;
using Blunt.Synthesis;

using SystemFloat = System.Single;

public class PhaserEffect : MonoBehaviour
{
	public float ldmin;
	public float ldmax;
	public float lfeedback;
	public float llfoFreq;

	public float rdmin;
	public float rdmax;
	public float rfeedback;
	public float rlfoFreq;

	public bool isEnabled;
	public float depth;
	private Phaser effect;

	void Start()
	{
		GameState.instance().onAudioLoad += onAudioLoad;
	}

	void onAudioLoad()
	{
		
		effect = new Phaser();
	}

	public void applyChanges()
	{
		effect.ap[0].dmin = Mathf.Clamp01(ldmin) * 0.5f;
		effect.ap[0].dmax = Mathf.Clamp01(ldmax) * 0.5f;
		effect.ap[0].feedback = Mathf.Clamp01(lfeedback) * 0.99f;
		effect.ap[0].lfoOmega = llfoFreq * EnvironmentTuning.hzToRads;

		effect.ap[1].dmin = Mathf.Clamp01(rdmin) * 0.5f;
		effect.ap[1].dmax = Mathf.Clamp01(rdmax) * 0.5f;
		effect.ap[1].feedback = Mathf.Clamp01(rfeedback) * 0.99f;
		effect.ap[0].lfoOmega = llfoFreq * EnvironmentTuning.hzToRads;

		effect.depth = Mathf.Clamp01(depth);
	}

	public void OnAudioFilterRead(SystemFloat[] data, int nChannels)
	{
		if(isEnabled && effect != null)
		{
			applyChanges();
			effect.process(data, data.Length / nChannels, nChannels, false);
		}

	}

	public class Phaser : SoundStage
	{
		const int numFilters = 6;
		public float depth;
		public class AllPassData
		{
			public float[] zm1 = new float[numFilters];
			public float[] a1 = new float[numFilters];
			public float dmin;
			public float dmax;
			public float feedback;
			public float lfoPhase;
			public float lfoOmega;
			public float state;
		}

		public AllPassData[] ap = { new AllPassData(), new AllPassData() };
		public void process(SystemFloat[] data, int nSampleFrames, int channels, bool channelIsEmpty)
		{
			unchecked
			{
				// nothing to do here
				if(channelIsEmpty)
					return;

				float sample;
				float delay, yAP, ytemp;
				// for each sample..

				float[] lfoScale = { (ap[0].dmax - ap[0].dmin) * 0.5f, (ap[1].dmax - ap[1].dmin) * 0.5f };

				for(int i = 0; i < nSampleFrames * channels; i += channels)
				{

					// for each channel..
					for(int k = 0; k < 2; ++k)
					{
						sample = data[i + k];
						// calculate sinusoidal delay
						delay = ap[k].dmin + (ap[k].dmax - ap[k].dmin) * (Mathf.Sin(ap[k].lfoPhase) + 1.0f) * 0.5f;
						// limit phase:
						ap[k].lfoPhase += ap[k].lfoOmega;
						if(ap[k].lfoPhase > Mathf.PI * 2)
							ap[k].lfoPhase -= Mathf.PI * 2;
						// generate all pass input
						yAP = sample + ap[k].state * ap[k].feedback;
						for(int z = numFilters - 1; z >= 0; --z)
						{
							// delay line
							ap[k].a1[z] = (1.0f - delay) / (1.0f + delay);
							// all pass
							ytemp = yAP * -ap[k].a1[z] + ap[k].zm1[z];
							ap[k].zm1[z] = ytemp * ap[k].a1[z] + yAP;
							// store iterate, update backwards
							yAP = ytemp;

						}
						ap[k].state = yAP;
						// output
						data[i + k] = sample + yAP * depth;
					}
					

				}
			}
	
		}
	}
}
