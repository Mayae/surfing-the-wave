﻿/*************************************************************************************

	blunt - a dull C# library - v. 0.1.0.

	Copyright (C) 2015 Janus Lynggaard Thorborg [LightBridge Studios]

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.

	See \licenses\ for additional details on licenses associated with this program.

*************************************************************************************/

#if UNITY_EDITOR
#define SYNTH_DEBUG
#endif

#define KILL_DENORMALS

#define V2
#define USE_SIMD
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using System.Runtime.CompilerServices;
using Mono.Simd;

namespace Blunt.Synthesis
{
	using DSPType = System.Single;
	using SystemFloat = System.Single;
	using v4sf = Vector4f;
	using v4si = Vector4i;
	using v4sui = Vector4ui;

	public class FMSynth : Sound.AudioConfiguration.AudioEventListener, Synthesizer, Sound.SoundStage
	{

		public interface SequenceCallback
		{
			void onSequencing(FMSynth fmSynth, PlayHead playHead);
		};

		public class Voice : Synthesis.Voice
		{
			public class Operator
			{
				public enum Kind
				{
					/// <summary>
					/// White noise generator.
					/// Ignores modulations
					/// </summary>
					Noise,
					/// <summary>
					/// Pure sine generator, modulations affect the phase directly,
					/// creating fm synthesis
					/// </summary>
					Sine,
					/// <summary>
					/// A variable Q/cutoff 12 dB lp/hp/bp filter, modulations
					/// affect cutoff as center * 2^(adsr * moddepth + modulations)
					/// </summary>
					Filter,
					/// <summary>
					/// see @setParameterGoal/value
					/// </summary>
					Parameter,
					/// <summary>
					/// This generates a random offset when the voice starts playing.
					/// Otherwise, follows the @Parameter system. Can be used to alter volume slightly in a random pattern, for instance.
					/// Essentially a memcpy operation, this is a very cheap operator.
					/// </summary>
					RandomOffset
				}

				public enum FilterTypes
				{
					LP,
					HP,
					BP
				}

				public enum Operation
				{
					Additive,  // adds to the previous modulator/voice operators
					Multiplicative,	// performs amplitude modulation on all the previous modulator/voice operators
				}

				/// <summary>
				/// The synthesized type (sines, squares etc.)
				/// </summary>
				public Kind kind;
				/// <summary>
				/// The operation mode of this oscillator.
				/// See @Operation
				/// </summary>
				public Operation op;
				/// <summary>
				/// The state (or phase) of the oscillator.
				/// </summary>
				public DSPType phase;
				/// <summary>
				/// The initial phase offset of this operator, on playback.
				/// </summary>
				public DSPType phaseOffset;
				/// <summary>
				/// The angular rotation in radians per sample of the oscillator.
				/// </summary>
				public DSPType omega;
				/// <summary>
				/// The envelope for this operator.
				/// </summary>
				public ADSREnvelope envelope;
				/// <summary>
				/// Volume of this oscillator. If the oscillator acts as an modulator, it represents
				/// the modulation amount.
				/// </summary>
				public DSPType volume;
				/// <summary>
				/// The mix between the voice buffer and the oscillator.
				/// For additive operations, this is equivalent to volume.
				/// However, operations such as FM replaces the previous buffer - this allows
				/// to intermix the buffers.
				/// </summary>
				public DSPType mix;
				/// <summary>
				/// Defines the relation in pitch of this operator.
				/// Ie., if the voice plays back at C4, and this operator
				/// has a pitch relation of 0.5f, it will play back at C3.
				/// </summary>
				public DSPType pitchRelation;
				/// <summary>
				/// Currently used for modulation depth of adsr on filters.
				/// For a value of 0.5f, the filter will sweep from start up to 50% away from
				/// fs/2.
				/// </summary>
				public DSPType modulationDepth;
				/// <summary>
				/// Whether the playback note affects this operators frequency.
				/// If not, remember to set the frequency at some point!
				/// </summary>
				public bool isFixed;
				/// <summary>
				/// Whether this operator is a modulator.
				/// Modulators are silent and their output gets operated (see @Operation) 
				/// into a temporary buffer. This buffer gets consumed as modulation by the next operator,
				/// that is not considered a modulator.
				/// </summary>
				public bool isModulator;
				public Filters.SVFBiquad filter;
				public FilterTypes filterType = FilterTypes.LP;
				public WhiteNoise noiseGenerator = new WhiteNoise();
				/// <summary>
				/// Used for Kind.RandomOffset. Default instance is Blunt.Utils.CheapRandom (through createAndAddRandomOffset()).
				/// This function is called each time the operator resets.
				/// </summary>
				public System.Func<DSPType> randomizer;

				/// <summary>
				/// The current parameter value.
				/// You shouldn't change this externally - use this.SetParameterValue()
				/// </summary>
				public DSPType currentParameterValue;
				/// <summary>
				/// The smoothstep filter.
				/// DO NOT CHANGE THIS PARAMETER EXTERNALLY.
				/// Use this.SetParameterGoal() or this.SetParameterValue()
				/// </summary>
				public Filters.SmoothStep smoothStepFilter = Filters.SmoothStep.constant(0);
				public Operator()
				{
					volume = 1.0f;
					envelope = new ADSREnvelope();
					envelope.setADSR(0, 0, 0, 0);
					kind = Kind.Sine;
					op = Operation.Additive;
					phaseOffset = phase = omega = 0;
					pitchRelation = 1;
					isFixed = false;
					isModulator = false;
					mix = 1.0f;
					currentParameterValue = 0.0f;
					modulationDepth = 0.0f;
				}
				/// <summary>
				/// Resets the operator to an initial state.
				/// </summary>
				public void reset()
				{
					envelope.stop();
					phase = phaseOffset;
					if(kind == Kind.RandomOffset)
						currentParameterValue = randomizer();

					filter.z1 = filter.z2 = 0;
				}

				/// <summary>
				/// This function is used in conjunction with Kind.Parameter for automating modulations.
				/// Parameters output can modulate the modulator buffer or the voice buffer
				/// and have no intrinsic phase/adsr/filter.
				/// They can however still be multiplicative or additive.
				/// Instead, on each process(), they will interpolate from a current value to a goal
				/// value, over timeInMS miliseconds.
				/// The idea is to allow external modulation of a certain parameter in a chained system, without
				/// altering the setup of a synth or a voice.
				/// It is safe to call this function from any thread, however changes may only take effect
				/// after @FMSynth.latency samples.
				/// Changes done in sequencing callbacks takes effect immediately.
				/// </summary>
				/// <param name="goal"></param>
				/// <param name="timeInMs"></param>
				public void parameterSetGoal(DSPType goal, DSPType timeInMs)
				{
					if(DSPType.IsInfinity(goal) || DSPType.IsNaN(goal))
                        return;
					float cval = currentParameterValue;
					Filters.SmoothStep ss = Filters.SmoothStep.design(goal, timeInMs);
					// setting these values allow the smoothing function 
					// to synchronize its value
					ss.currentValue = ss.startingValue = cval;
					// atomically assign reference.
					smoothStepFilter = ss;
                }
				/// <summary>
				/// Directly sets the parameterValue (no smoothing) and stops any interpolation.
				/// </summary>
				/// <param name="value"></param>
				public void parameterSetValue(DSPType value)
				{
					float cval = currentParameterValue;
					Filters.SmoothStep ss = Filters.SmoothStep.constant(value);
					// setting these values allow the smoothing function 
					// to synchronize its value
					ss.currentValue = ss.startingValue = cval;
					// atomically assign reference.
					smoothStepFilter = ss;
				}
			}
			/// <summary>
			/// Use this to avoid the synth killing your voice when it is silent for too long.
			/// </summary>
			public bool dontKillOnSilence;
			public List<Operator> operators;
			public FMSynth hostSynth;
			public Voice(FMSynth s)
				: base(s)
			{
				operators = new List<Operator>();
				hostSynth = s;
				dontKillOnSilence = false;
			}

			public override void release()
			{

				foreach(Operator o in operators)
				{
					o.envelope.release();
				}
			}

			public override void stop()
			{
				enabled = false;
				foreach(Operator o in operators)
				{
					o.envelope.stop();
				}

			}

			public override void play(float volumeInDBs)
			{
				enabled = false;
				this.volume = MathExt.dbToFraction(volumeInDBs);
				foreach(Operator o in operators)
				{
					o.reset();
				}
				timeAtPlay = (float)hostSynth.getPlayHead().timeInSeconds;
				timeSinceLastThreshold = 0;
				enabled = true;
			}

			public override void play()
			{
				enabled = false;
				foreach(Operator o in operators)
				{
					o.reset();
				}
				timeAtPlay = (float)hostSynth.getPlayHead().timeInSeconds;
				timeSinceLastThreshold = 0;
				enabled = true;
			}


			public Operator createAndAddOperator()
			{
				var o = new Operator();
				operators.Add(o);
				return o;
			}

			public Operator createAndAddRandomOffset(System.Func<DSPType> randomizer)
			{
				var o = new Operator();
				o.kind = Operator.Kind.RandomOffset;
				o.isFixed = true;
				o.randomizer = randomizer;
				operators.Add(o);
				return o;
			}

			public Operator createAndAddRandomOffset()
			{
				return createAndAddRandomOffset(new Blunt.Utils.CheapRandom().random11);
			}

			public Operator createAndAddFilter(Operator.FilterTypes filterType, DSPType cutoff, DSPType Q)
			{
				var o = new Operator();
				o.kind = Operator.Kind.Filter;
				o.filterType = filterType;
				o.isFixed = true;
				o.filter.design(cutoff, Q);
				o.envelope.isVCA = false;
				operators.Add(o);
				return o;
			}

			public Operator createAndAddParameter( DSPType initialValue)
			{
				var o = new Operator();
				o.kind = Operator.Kind.Parameter;
				o.isFixed = true;
				operators.Add(o);
				o.parameterSetValue(initialValue);
				return o;
			}

			public override void setPitch(DSPType freq)
			{
				foreach(Operator o in operators)
				{
					if(!o.isFixed)
						o.omega = (DSPType)(o.pitchRelation * 2 * System.Math.PI * freq / EnvironmentTuning.sampleRate);
				}
			}
		}
		/// <summary>
		/// This defines the maximum latency of the system in samples - it may be lower.
		/// This is also effectively the maximum rate of which voices, operators and sequencing
		/// is triggered.
		/// </summary>
		private int latency = 128;
		/// <summary>
		/// The accumulated and operated current buffer of the voice
		/// </summary>
		static private CResizableContainer<SystemFloat> accumulatedVoiceBuffer
			= new CResizableContainer<SystemFloat>();
		/// <summary>
		/// The buffer for the current envelope calculations.
		/// </summary>
		static private CResizableContainer<SystemFloat> envelopeBuffer
			= new CResizableContainer<SystemFloat>();
		/// <summary>
		/// The buffer for the current envelope calculations.
		/// </summary>
		static private CResizableContainer<SystemFloat> modulatorBuffer
			= new CResizableContainer<SystemFloat>();
		
		private List<Voice> voices = new List<Voice>();
		private List<SequenceCallback> sequencers = new List<SequenceCallback>();
		/// <summary>
		/// If a voice's output RMS for @latency frames is below this threshold, 
		/// it gets marked silent for a period of time.
		/// If this period exceeds @numSamplesToCut, the voice gets cut.
		/// </summary>
		private DSPType cutoffThreshold;
		/// <summary>
		/// See @cutoffThreshold.
		/// </summary>
		private int numSamplesToCut;
		// only for display!
		public int numVoices = 0;
		public int numActiveVoices = 0;
		public int numActiveOperators = 0;
		/// <summary>
		/// When using exponential decays, the magnitude of the numbers keep getting smaller
		/// until a point where they become denormal and virtually destroy performance.
		/// This threshold flushes numbers to zero if the become smaller.
		/// </summary>
		public float denormalFlush = MathExt.dbToFraction(-96)/*float.Epsilon*/;
		/// <summary>
		/// The frequency modulation index for fm synthesis.
		/// </summary>
		public float fmIndex = 13.0f;
		/// <summary>
		/// The playhead position in time.
		/// </summary>
		private PlayHead playHead = new PlayHead();

		// polynomial constants.
		v4sf polyB = new v4sf((float)(4 / Math.PI));
		v4sf polyP = new v4sf(0.225f);
		v4sf polyC = new v4sf((float)(-4 / (Math.PI * Math.PI)));

		public void setLatency(int newLatency)
		{
			latency = Mathf.NextPowerOfTwo(newLatency);
		}

		public FMSynth () 
		{
			EnvironmentTuning.updateFields ();
			setCutThreshold (-96);
			numSamplesToCut = (int)EnvironmentTuning.sampleRate / 4;
		}
			
		public PlayHead getPlayHead()
		{
			return playHead;
		}
			
		public void setCutThreshold(DSPType cutoffInDBs)
		{
			cutoffThreshold = (DSPType)Math.Abs (MathExt.dbToFraction(cutoffInDBs));
		}
			
		public Synthesis.Voice createVoice(int midiNote, DSPType detuneInCents = 0)
		{
			Voice v = new Voice (this);
			v.midiNote = midiNote;
			voices.Add (v);
			numVoices++;
			return v;
		}
			
		public Synthesis.Voice createVoice()
		{
			Voice v = new Voice (this);
			voices.Add(v);
			numVoices++;
			return v;
		}


		public void addVoice(Synthesis.Voice newVoice)
		{
			Voice v = newVoice as Voice;
			if(v != null)
			{
				var voicelist = new List<Voice>(voices);
				voicelist.Add(v);
				voices = voicelist;
			}
		}

		public void removeVoice(Synthesis.Voice newVoice)
		{
			Voice v = newVoice as Voice;
			if(v != null)
			{
				var voicelist = new List<Voice>(voices);
				voicelist.Remove(v);
				voices = voicelist;
			}
		}

		public void addSequencer(SequenceCallback cb)
		{
			var seqs = new List<SequenceCallback>(sequencers);
			seqs.Add(cb);
			// atomically change array
			sequencers = seqs;
		}
		public void removeSequencer(SequenceCallback cb)
		{
			var seqs = new List<SequenceCallback>(sequencers);
			seqs.Remove(cb);
			// atomically change array
			sequencers = seqs;
		}

		private void doSequencing(int nSamples)
		{
			var seqs = this.sequencers;

			for(int i = 0; i < seqs.Count; ++i)
			{
				seqs[i].onSequencing(this, playHead);
			}
				
			playHead.advance(nSamples);
		}




		public void process(SystemFloat[] data, int nSampleFrames, int channels, bool channelIsEmpty)
		{

			// k is the synths own counter between voices.
			// n is the counter for operators, starting at zero to latency samples.
			// j is the counter for envelope and mixing stages.
			// i is the counter for copying Voice buffer into the main buffer.
			int k = 0;
			int nextStop = 0;
			int nTotalFrames = nSampleFrames * channels;
			int maxLatency = System.Math.Min(nSampleFrames, latency);
			int step = maxLatency * channels; ;

#if SYNTH_DEBUG
			int currentActiveVoices = 0;
			int currentActiveOperators = 0;
#endif

			accumulatedVoiceBuffer.ensureSize(maxLatency);
			envelopeBuffer.ensureSize(maxLatency);
			modulatorBuffer.ensureSize(maxLatency);

			var voiceList = voices;

			// okay, so here we loop over the whole buffer in <latency> steps
			// this is done so we cant adjust the rate of adding/disabling notes
			// with runtime-adjustable quantization.
			do
			{
#if SYNTH_DEBUG
				currentActiveVoices = 0;
				currentActiveOperators = 0;
#endif
				doSequencing(maxLatency);
				bool previousWasModulator = false;
				nextStop = k + step;

				int diff = nTotalFrames - nextStop;
				if(diff < 0)
					nextStop += diff;	// (diff is negative, hence addition, but we subtract the difference, really)

				for(int activeVoice = 0; activeVoice < voiceList.Count; ++activeVoice)
				{
					var v = voiceList[activeVoice];
					if(v == null)
					{
						Debug.Log("Null voice in FMSynth.List!!");
						continue;
					}

					if(!v.enabled)
						continue;
#if SYNTH_DEBUG
					currentActiveVoices++;
#endif
					accumulatedVoiceBuffer.clearRange(0, maxLatency);

					// the accumulated buffer for this voice.
					var voiceBuf = accumulatedVoiceBuffer.getData();
					// each operator must store its output in this buffer
					var modBuf = modulatorBuffer.getData();
					Array.Clear(modBuf, 0, maxLatency);
					////////////////////////////////////////////////////////////
					// Loop over each oscillator, synthesizing it, calculating
					// its operation, enveloping it and adding it to the voicebuffer
					// note: most of this operation is moved into seperate functions
					// now.
					////////////////////////////////////////////////////////////
					var operators = v.operators;

					for(int y = 0; y < operators.Count; ++y)
					{
						var o = operators[y];
#if SYNTH_DEBUG
						currentActiveOperators++;
#endif

						var destBuf = o.isModulator ? modBuf : voiceBuf;

						// synthesize the next batch
						switch(o.kind)
						{
							case Voice.Operator.Kind.Sine:
								synthesizeBatch(o, modBuf, destBuf, maxLatency);
								break;
							case Voice.Operator.Kind.Filter:
								filterBatch(o, voiceBuf, modBuf, destBuf, maxLatency);
								break;
							case Voice.Operator.Kind.Parameter:
								parameterBatch(o, destBuf, maxLatency);
								break;
							case Voice.Operator.Kind.Noise:
								noiseBatch(o, destBuf, maxLatency);
								break;
							case Voice.Operator.Kind.RandomOffset:
								randomOffsetBatch(o, destBuf, maxLatency);
								break;
						}

						if(o.isModulator)
						{
							previousWasModulator = true;
						}
						else
						{
							// generators consume all modulation.
							if(previousWasModulator)
								Array.Clear(modBuf, 0, maxLatency);
							previousWasModulator = false;
						}

					}	// for each oscillator

					////////////////////////////////////////////////////////////
					// Mixing of voiceList
					////////////////////////////////////////////////////////////
					DSPType averageOutput = 0;
					for(int i = k, n = 0; i < nextStop; i += channels, n++)
					{
						var sample = voiceBuf[n] * v.volume;
						averageOutput += sample * sample;
						data[i] += sample;
						data[i + 1] += sample;
#if SYNTH_DEBUG && KILL_DENORMALS
						// do not want this to happen...
						if(sample > 0 && sample < 1e-35f)
							Debug.Log("Denormal numbers...");
#endif
					}

					////////////////////////////////////////////////////////////
					// Cutting of voiceList.
					////////////////////////////////////////////////////////////
					if(!v.dontKillOnSilence)
					{
						averageOutput = Mathf.Sqrt((float)averageOutput) / maxLatency;	 // (number of samples this round).

						if(cutoffThreshold > averageOutput)
						{
							v.timeSinceLastThreshold += maxLatency;
							if(v.timeSinceLastThreshold > numSamplesToCut)
							{
								// cut the voice.
								v.enabled = false;
							}
						}
						else
						{
							// reset the timer, the voice lives on for a bit more!!
							v.timeSinceLastThreshold = 0;
						}
					}

				}	// for each voice

				k = nextStop;

			} while(k < nTotalFrames);	// latency/sample loop

#if SYNTH_DEBUG
			numActiveVoices = currentActiveVoices;
			numActiveOperators = currentActiveOperators;
#endif
		}

		public void noiseBatch(Voice.Operator o, SystemFloat[] output, int nSamples)
		{
			const int loopSize = 4;
			// noise generations..
			v4sf vOut;
			v4sf vThree = new v4sf(3.0f);
			v4sf vVol = new v4sf(o.volume);
			v4sui vSeed = new v4sui(o.noiseGenerator.seed[0], o.noiseGenerator.seed[1], o.noiseGenerator.seed[2], o.noiseGenerator.seed[3]);
			v4sui vC1 = new v4sui(196314165U);
			v4sui vC2 = new v4sui(907633515U);
			v4sui vExponent = new v4sui(0x40000000);
			v4sui vMantissa = new v4sui(0x7FFFFF);
			v4sui vWhite;

			// ADSR variables
			ADSREnvelope e = o.envelope; // ensure its a reference..
			DSPType recAttack = e.reciprocalAttack;
			DSPType inAttack = e.deltaSamples * recAttack;

			// as the decay stage approaches the sustain, it adds the filter to the sustain
			// instead. When we move to release, we add the sustain,
			// because release does an indefinite decay on the total level.
			if(e.shouldMoveToRelease && !e.didMoveToRelease)
			{
				e.didMoveToRelease = true;
				e.stage = ADSREnvelope.Stage.Release;
			}

			v4sf vattackStop = new v4sf(e.stage != ADSREnvelope.Stage.Attack ? inAttack - recAttack : 1.0f);
			v4sf vAttack = new v4sf(inAttack, inAttack + recAttack, inAttack + recAttack * 2, inAttack + recAttack * 3);
			v4sf vRecipAttack = new v4sf(recAttack * 4);
			v4sf vAttackStageMask = v4sf.Zero, vADSR = v4sf.Zero;

			float filterCoeff = (e.stage == ADSREnvelope.Stage.Release) ? e.releasePole : e.decayPole;
			v4sf vSustainOffset = new v4sf(e.stage == ADSREnvelope.Stage.Release ? 0.0f : e.sustainLevel);
			v4sf vFilterCoeff = new v4sf(filterCoeff * filterCoeff * filterCoeff * filterCoeff);

			v4sf vCoefficient;
			float projectedAttack = e.auxVol;
			if(e.stage == ADSREnvelope.Stage.Attack && inAttack + nSamples * recAttack > 1.0f)
			{
				projectedAttack = 1.0f;
			}

			float startingVolume = e.stage == ADSREnvelope.Stage.Release ? projectedAttack : projectedAttack - e.sustainLevel;
			v4sf vVolume = new v4sf(startingVolume, startingVolume * filterCoeff,
				startingVolume * filterCoeff * filterCoeff, startingVolume * filterCoeff * filterCoeff * filterCoeff);
			// mixing variables
			v4sf vIn;
			v4sf vMulSelector =	/*(o.op == Voice.Operator.Operation.Multiplicative) ? SimdExt.vAllBits :*/
				SimdExt.vAllBits;
			if(o.op != Voice.Operator.Operation.Multiplicative)
				vMulSelector = SimdExt.vNoBits;
			v4sf vMulC;
			v4sf vMix = new v4sf(o.mix);
			v4sf vInvMix = new v4sf(1 - o.mix);
			v4sf vOne = SimdExt.vOne;
			v4sf vDenormalFlush = new v4sf(EnvironmentTuning.denormalFlush);
			for(int i = 0; i < nSamples; i += loopSize)
			{
				////////////////////////////////////////////////////////////
				// Synthesis
				////////////////////////////////////////////////////////////
				vIn = ArrayExtensions.GetVector(output, i);

				vSeed = (vSeed * vC1) + vC2;
				// so we actually need a logical right shift here
				// (arithmetic sign-extends the sign bit)
				// mono includes a logical shift - but it is only for longs (?!)
				vWhite = VectorOperations.ArithmeticRightShift(vSeed, 9);
				// basically we just mask out the exponent and the signbit, saving
				// the mantissa only:
				vWhite = (vWhite & vMantissa) | vExponent;
				vOut = (((v4sf)vWhite) - vThree);

				////////////////////////////////////////////////////////////
				// ADSR
				////////////////////////////////////////////////////////////
				// is attack still below the goal? if, mask out release and decay.
				vAttackStageMask = VectorOperations.CompareNotLessEqual(vattackStop, vAttack);
				// select attack or release/decay
				vADSR = vAttack & vAttackStageMask | VectorOperations.AndNot(vAttackStageMask, vVolume + vSustainOffset);
				// this ensures the coefficient is one (thus, not decreasing the currentvolume)
				// as long as attack has not reached the goal.
				vCoefficient = vAttackStageMask & vOne | VectorOperations.AndNot(vAttackStageMask, vFilterCoeff);
				// increase attack.
				vAttack += vRecipAttack;
				// decay / release:
				vVolume = vVolume * vCoefficient;
#if KILL_DENORMALS
				vVolume = vVolume & VectorOperations.CompareLessThan(vDenormalFlush, vVolume);
#endif
				////////////////////////////////////////////////////////////
				// Mixing
				////////////////////////////////////////////////////////////
				// select amplitude modulation coefficient:
				vMulC = vIn & vMulSelector | VectorOperations.AndNot(vMulSelector, vOne);
				// mix between additive/multiplicative mix of this operator and the old stuff
				// in the buffer
				vOut = vOut * vMulC * vMix * vADSR * vVol + vIn * vInvMix;

				// store output
				ArrayExtensions.SetVector(output, vOut, i);

			}
				
			o.noiseGenerator.seed[0] = vSeed.X;
			o.noiseGenerator.seed[1] = vSeed.Y;
			o.noiseGenerator.seed[2] = vSeed.Z;
			o.noiseGenerator.seed[3] = vSeed.W;
			// if attack mask is set, we can move on to decay. 
			// note, this is automatically selected in the loop.
			vAttackStageMask = VectorOperations.AndNot(vAttackStageMask, v4sf.One);
			if((vAttackStageMask.W) > 0.0f && e.stage == ADSREnvelope.Stage.Attack)
			{
				e.stage = ADSREnvelope.Stage.Decay;
			}
			e.auxVol = vADSR.W;
			e.deltaSamples += nSamples;
			e.deltaSamples = e.deltaSamples > e.attackTime ? e.attackTime : e.deltaSamples;

		}

		public void randomOffsetBatch(Voice.Operator o, SystemFloat[] output, int nSamples)
		{
			const int loopSize = 4;

			v4sf vVol = new v4sf(o.volume);
			v4sf vY;
			v4sf vValue = new v4sf(o.currentParameterValue);
			// mixing variables
			v4sf vIn;
			v4sf vMulSelector =	/*(o.op == Voice.Operator.Operation.Multiplicative) ? SimdExt.vAllBits :*/
				SimdExt.vAllBits;
			if(o.op != Voice.Operator.Operation.Multiplicative)
				vMulSelector = SimdExt.vNoBits;
			v4sf vMulC;
			v4sf vMix = new v4sf(o.mix);
			v4sf vInvMix = new v4sf(1.0f - o.mix);
			v4sf vOne = SimdExt.vOne;

			for(int i = 0; i < nSamples; i += loopSize)
			{
				// get the 'input'
				vIn = ArrayExtensions.GetVector(output, i);
				////////////////////////////////////////////////////////////
				// Mixing
				////////////////////////////////////////////////////////////
				// select amplitude modulation coefficient:
				vMulC = vIn & vMulSelector | VectorOperations.AndNot(vMulSelector, vOne);
				// mix between additive/multiplicative mix of this operator and the old stuff
				// in the buffer
				vY = vValue * vMulC * vMix * vVol + vIn * vInvMix;

				// store output
				ArrayExtensions.SetVector(output, vY, i);
			}

		}

		public void parameterBatch(Voice.Operator o, SystemFloat[] output, int nSamples)
		{
			const int loopSize = 4;

			// see @filter.process
			Filters.SmoothStep filter = o.smoothStepFilter;
			// since this filter is not 'recursive'/iir-oriented, we can easily
			// simd accelerate it.
			// firstly, sanity check updates:
			if(filter.currentValue != o.currentParameterValue)
			{
				// okay, an update to the filter occured, and they got the wrong value.
				// we simply update the fields here and 'reset' the interpolation.
				filter.currentValue = filter.startingValue = o.currentParameterValue;
			}
			//Debug.Log("current parameter: " + filter.currentValue + ", progress: " + filter.deltaSamples * filter.omega + ", goal = " + filter.goal);
			v4sf vVol = new v4sf(o.volume);
			v4sf vY = new v4sf();
			v4sf vFilterOut = new v4sf();
			v4sf vPhase = new v4sf(filter.deltaSamples, filter.deltaSamples + 1, filter.deltaSamples + 2, filter.deltaSamples + 3);
			v4sf vOmega = new v4sf(filter.omega);
			v4sf vEndValue = new v4sf(filter.goal);
			v4sf vStartValue = new v4sf(filter.startingValue);
			// mixing variables
			v4sf vIn;
			v4sf vMulSelector =	/*(o.op == Voice.Operator.Operation.Multiplicative) ? SimdExt.vAllBits :*/
				SimdExt.vAllBits;
			if(o.op != Voice.Operator.Operation.Multiplicative)
				vMulSelector = SimdExt.vNoBits;
			v4sf vMulC;
			v4sf vMix = new v4sf(o.mix);
			v4sf vInvMix = new v4sf(1.0f - o.mix);
			v4sf vOne = SimdExt.vOne;
			v4sf vTwo = new v4sf(2f);
			v4sf vThree = new v4sf(3f);
			v4sf vFour = new v4sf(4f);
			for(int i = 0; i < nSamples; i += loopSize)
			{
				// get the 'input'
				vIn = ArrayExtensions.GetVector(output, i);
				////////////////////////////////////////////////////////////
				// Synthesis
				////////////////////////////////////////////////////////////
				// compute the fraction:
				// (and cap it at one).
				vY = VectorOperations.Min(vPhase * vOmega, vOne);
				// compute the smoothed step:
#if INTERPOLATE_SMOOTHLY
				vY = (vY * vY) * (vThree - vTwo * vY);
#endif
				// this is all very serial, so don't even bother to use different variables.
				vFilterOut = vEndValue * vY + vStartValue * (vOne - vY);
				// increase the phase-
				vPhase += vFour;
				////////////////////////////////////////////////////////////
				// Mixing
				////////////////////////////////////////////////////////////
				// select amplitude modulation coefficient:
				vMulC = vIn & vMulSelector | VectorOperations.AndNot(vMulSelector, vOne);
				// mix between additive/multiplicative mix of this operator and the old stuff
				// in the buffer
				vY = vFilterOut * vMulC * vMix * vVol + vIn * vInvMix;
	
				// store output
				ArrayExtensions.SetVector(output, vY, i);
			}

			filter.deltaSamples += nSamples;
			if(filter.deltaSamples * filter.omega > 1)
				filter.deltaSamples = Mathf.RoundToInt(1.0f / filter.omega);
			filter.currentValue = vFilterOut.W;
			if(filter.goal > 0 && filter.currentValue > o.volume * filter.goal)
			{
				int x = 0;
				//Debug.Log(x);
			}
			o.currentParameterValue = filter.currentValue;

		}

		public void filterBatch(Voice.Operator o, SystemFloat[] input, SystemFloat[] mod, SystemFloat[] output, int nSamples)
		{
			const int loopSize = 4;

			// calculate filter adsr modulation depth
			// for a modulation depth of 100%, if the filter is centered at 0.2 nf,
			// the adsr must raise the center up to 1:
			//	1 = fc * 2^x
			//	== ln(1/fc)/ln(2) = x = vModIndex
			// finalFilterFreq = pi * centerFreq * 2^(modulationDepth * adsr * x + modulation)

			v4sf vModDepth = new v4sf(Mathf.Log(1f / o.filter.normalizedFrequency)) * SimdExt.vLN2Rec;
			vModDepth = vModDepth * new v4sf(o.modulationDepth);
			// since filters per nature are recursive, we cannot parallize the computations in serial,
			// however modulation and coefficient calculations lends themselves very well 
			// to dirty accelerated computations (who cares about +/- 0.5% tuning error for
			// modulations?). note that only the actual 'finalFilterFreq' will be dirty calculated,
			// coefficients are calculated in 'high' precision.
			// and then we can also utilize nice IEEE hacks and bad approximations!
			// the mixing stage will be done in simd also.
			v4sf vQRec = new v4sf(1.0f / o.filter.Q);
			v4sf vOne = SimdExt.vOne;
			v4sf vTanC1 = new v4sf(2.00033e-2f);
			v4sf vTanC2 = new v4sf(3.1755e-1f);
			v4sf vModulation;
			v4sf vTanApp = new v4sf(o.filter.f);
			v4sf vTanSquare;
			v4sui vBase2;
			v4sf vFilterCenter = new v4sf(o.filter.normalizedFrequency) * SimdExt.vPi;
			v4sf vClip = new v4sf(-126f);
			v4sf vMagicNumber = new v4sf(126.94269504f);
			v4sf vScale = new v4sf(8388608f); // 1 << 23

			// compute the envelope (this can be moved inside this loop, at some point)
			var envBuf = envelopeBuffer.getData();
			o.envelope.processBatch(envBuf, nSamples);

			// select the correct filter tap:
			v4sf vLP = SimdExt.vNoBits, vHP = SimdExt.vNoBits, vBP = SimdExt.vNoBits;
			if(o.filterType == Voice.Operator.FilterTypes.LP)
				vLP = SimdExt.vAllBits;
			else if(o.filterType == Voice.Operator.FilterTypes.HP)
				vHP = SimdExt.vAllBits;
			else
				vBP = SimdExt.vAllBits;

			// output filter coefficient destinations.
			float[] g = new float[4], r = new float[4], f = new float[4];
			uint[] store = new uint[4];
			float z1 = o.filter.z1, z2 = o.filter.z2;
			float hp, bp, lp;
			float scalarF;
			const float denormalKiller = 10e-12f;
			// mixing variables
			v4sf vIn = v4sf.Zero;
			v4sf vMulSelector =	/*(o.op == Voice.Operator.Operation.Multiplicative) ? SimdExt.vAllBits :*/
				SimdExt.vAllBits;
			if(o.op != Voice.Operator.Operation.Multiplicative)
				vMulSelector = SimdExt.vNoBits;
			v4sf vMulC;
			v4sf vMix = new v4sf(o.mix);
			v4sf vInvMix = new v4sf(1.0f - o.mix);
			v4sf vVol = new v4sf(o.volume);
#if KILL_DENORMALS
			input[0] += denormalKiller;
#endif
			
			for(int i = 0; i < nSamples; i += loopSize)
			{

				// modulation are 2^x * center 
				// that means, filters centrered at 440 hz with a pure sine modulator at A = 1
				// will modulate between 220 and 880hz
				// compute mod = modDepth * modIndex * adsr + modulation
				vModulation = VectorOperations.Max(ArrayExtensions.GetVector(mod, i), vClip);

				vModulation += ArrayExtensions.GetVector(envBuf, i) * vModDepth;
				// compute 2^mod
                vModulation = (vModulation + vMagicNumber) * vScale;

				// MONO HAS NO INTEGER CONVERSION FHJDLKSÆJFLÆK JALKÆF
#if UINT_CAST
				// for some reason... this may give 800% performance penalties.. it is the correct solution, though.
				vBase2 = new v4sui((uint)vModulation.X, (uint)vModulation.Y, (uint)vModulation.Z, (uint)vModulation.W);
#else
				vBase2 = (v4sui)new v4si((int)vModulation.X, (int)vModulation.Y, (int)vModulation.Z, (int)vModulation.W);
				/*ArrayExtensions.SetVector(f, vModulation, 0);
				store[0] = (uint)(int)f[0]; store[1] = (uint)(int)f[1]; store[2] = (uint)(int)f[2]; store[3] = (uint)(int)f[3];
				vBase2 = ArrayExtensions.GetVector(store, 0);*/
#endif
				//#if !KILL_DENORMALS
				// compute fc * 2^mod
				vTanApp = ((v4sf)vBase2) * vFilterCenter;
				// calculate tan(pi * fc * 2^mod) 
				// approximated by this: (((((x*x) * 2.00033^-2) + 3.1755^-1)* (x*x)) + 1) * x
				// since the former is in fact a polynomial, it will never reach tan(pi), which is +infinity
				// it comes close though, with a small tuning error towards nyquist. we can also utilize this
				// so we don't have to worry about range reduction
//#endif
				vTanSquare = vTanApp * vTanApp;
				vTanApp = ((vTanSquare * vTanC1 + vTanC2) * vTanSquare + vOne) * vTanApp;
				// store f coeff (tan(pi * fc * 2^mod)

				ArrayExtensions.SetVector(f, vTanApp, 0);
				// you can debug the filter coefficient here
				//ArrayExtensions.SetVector(output, vTanApp * new v4sf(10), i);
				// vModulation is now r
				vModulation = vTanApp + vQRec;
				ArrayExtensions.SetVector(r, vModulation, 0);
				// vModulation is now g
				vModulation = vOne / (vTanApp * vModulation + vOne);
				// store g
				ArrayExtensions.SetVector(g, vModulation, 0);
				// fetch input now

				vIn = ArrayExtensions.GetVector(input, i);

				// now all filter coefficients are stored into f, g, r for the next 4 samples.
				// then we basically run the filter 4 times now.
				// we store output lp in f, output bp in g, output hp in r.

				// filtering
#if KILL_DENORMALS
				// the problem is z1 and z2 are decaying exponentially.
				// however, we cannot flush them to zero because that screws up the sound.
				// instead, we add a little dc offset at the start of the sample batch.
				/*z1 += denormalKiller;
				z1 -= denormalKiller;
				z2 += denormalKiller;
				z1 -= denormalKiller;*/
#endif
				scalarF = f[0];
				hp = (input[i] - r[0] * z1 - z2) * g[0];
				bp = z1 + scalarF * hp;
				lp = z2 + scalarF * bp;
				// update of delay-state variables
				z1 += 2 * scalarF * hp;
				z2 += 2 * scalarF * bp;
				// store output filter
				f[0] = lp;
				r[0] = bp;
				g[0] = hp;
				// now the rest of the runs...
				scalarF = f[1];
				hp = (input[i + 1] - r[1] * z1 - z2) * g[1];
				bp = z1 + scalarF * hp;
				lp = z2 + scalarF * bp;
				z1 += 2 * scalarF * hp;
				z2 += 2 * scalarF * bp;
				f[1] = lp; r[1] = bp; g[1] = hp;
				scalarF = f[2];
				hp = (input[i + 2] - r[2] * z1 - z2) * g[2];
				bp = z1 + scalarF * hp;
				lp = z2 + scalarF * bp;
				z1 += 2 * scalarF * hp;
				z2 += 2 * scalarF * bp;
				f[2] = lp; r[2] = bp; g[2] = hp;
				scalarF = f[3];
				hp = (input[i + 3] - r[3] * z1 - z2) * g[3];
				bp = z1 + scalarF * hp;
				lp = z2 + scalarF * bp;
				z1 += 2 * scalarF * hp;
				z2 += 2 * scalarF * bp;
				f[3] = lp; r[3] = bp; g[3] = hp;

				////////////////////////////////////////////////////////////
				// Mixing
				////////////////////////////////////////////////////////////
				// select amplitude modulation coefficient and the correct filter tap:
				// total output is now vModulation
				vModulation = ArrayExtensions.GetVector(f, 0) & vLP;
				vMulC = vIn & vMulSelector | VectorOperations.AndNot(vMulSelector, vOne);
				vModulation |= ArrayExtensions.GetVector(r, 0) & vBP;
				vModulation |= ArrayExtensions.GetVector(g, 0) & vHP;


				// mix between additive/multiplicative mix of this operator and the old stuff
				// in the buffer
				vModulation = vModulation * vMulC * vMix * vVol + vIn * vInvMix;

				// store output
				ArrayExtensions.SetVector(output, vModulation, i);

			}
			// store filter state 
			o.filter.z1 = z1;
			o.filter.z2 = z2;
		}



		public void synthesizeBatch(Voice.Operator o, SystemFloat[] mod, SystemFloat[] output, int nSamples)
		{
			unchecked
			{
				const int vectorSize = 4;
				// if anything crashes inside this method on runtime, it is the mono runtime crashing
				// while compiling this method. it's simd handling is very immature, try reordering
				// stuff or change expressions.
#if SYNTH_DEBUG
				if((nSamples & (vectorSize - 1)) != 0)
					Debug.Log("Warning: FMSynth::generateBatch recieved a datasize which is not a multiple of " + vectorSize + ".");
#endif

				// polynomial approximation variables
				v4sf B = polyB, C = polyC, P = polyP;
				v4sf vSignMask = SimdExt.vSignMask;
				v4sf vPhase = new v4sf(o.phase, o.phase + o.omega, o.phase + o.omega * 2, o.phase + o.omega * 3);
				v4sf vModPhase = v4sf.Zero;
				v4sf vPi = v4sf.Pi;
				v4sf vRotation = new v4sf(o.omega * 4);
				v4sf vVol = new v4sf(o.volume), vY;
				v4sf vPiTwo = SimdExt.vPiTwo;
				v4sf vPhaseWrap = SimdExt.vPiTwoRec;
				v4sf vModulo;
				v4sf vFMIndex = new v4sf(fmIndex);
				v4sf vOne = SimdExt.vOne;
				v4sf vFakeNInfRound = new v4sf(64f);
				// ADSR variables
				ADSREnvelope e = o.envelope; // ensure its a reference..
				DSPType recAttack = e.reciprocalAttack;
				DSPType inAttack = e.deltaSamples * recAttack;

				// as the decay stage approaches the sustain, it adds the filter to the sustain
				// instead. When we move to release, we add the sustain,
				// because release does an indefinite decay on the total level.
				if(e.shouldMoveToRelease && !e.didMoveToRelease)
				{
					e.didMoveToRelease = true;
					e.stage = ADSREnvelope.Stage.Release;
				}

				v4sf vattackStop = new v4sf(e.stage != ADSREnvelope.Stage.Attack ? inAttack - recAttack : 1.0f);
				v4sf vAttack = new v4sf(inAttack, inAttack + recAttack, inAttack + recAttack * 2, inAttack + recAttack * 3);
				v4sf vRecipAttack = new v4sf(recAttack * 4);
				v4sf vAttackStageMask = v4sf.Zero, vADSR = v4sf.Zero;

				float filterCoeff = (e.stage == ADSREnvelope.Stage.Release) ? e.releasePole : e.decayPole;
				v4sf vSustainOffset = new v4sf(e.stage == ADSREnvelope.Stage.Release ? 0.0f : e.sustainLevel);
				v4sf vFilterCoeff = new v4sf(filterCoeff * filterCoeff * filterCoeff * filterCoeff);

				v4sf vCoefficient;
				float projectedAttack = e.auxVol;
				if(e.stage == ADSREnvelope.Stage.Attack && inAttack + nSamples * recAttack > 1.0f)
				{
					projectedAttack = 1.0f;
				}

				float startingVolume = e.stage == ADSREnvelope.Stage.Release ? projectedAttack : projectedAttack - e.sustainLevel;
				v4sf vVolumeMul = new v4sf(1.0f, filterCoeff, filterCoeff * filterCoeff, filterCoeff * filterCoeff * filterCoeff);
				v4sf vVolume = new v4sf(startingVolume) * vVolumeMul;

				// mixing variables
				v4sf vIn;
				v4sf vMulSelector = /*(o.op == Voice.Operator.Operation.Multiplicative) ? SimdExt.vAllBits :*/ 
					SimdExt.vAllBits;
				if(o.op != Voice.Operator.Operation.Multiplicative)
					vMulSelector = SimdExt.vNoBits;
				v4sf vMulC;
				v4sf vMix = new v4sf(o.mix);
				v4sf vInvMix = new v4sf(1 - o.mix);
				v4sf vDenormalFlush = new v4sf(EnvironmentTuning.denormalFlush);

				for(int i = 0; i < nSamples; i += vectorSize)
				{
					////////////////////////////////////////////////////////////
					// Synthesis
					////////////////////////////////////////////////////////////
					// compute the phase modulation and scale by the FM index
					vModPhase = vPhase + ArrayExtensions.GetVector(mod, i) * vFMIndex;
					// scale phase such that 2pi = 1, cast to int (floor) to remove fraction
					vModulo = vPhaseWrap * vModPhase;
					// add a constant so we avoid int truncation towards zero
					vModulo += vFakeNInfRound;
					//vModulo -= new v4sf(0.5f);
					// this next line eats 60% of performance, but unfortunately our version of Mono
					// doesn't include the native simd conversion functions... sigh
					vModulo = new v4sf((int)vModulo.X, (int)vModulo.Y, 
						(int)vModulo.Z, (int)vModulo.W);
					// remove the constant again.
					vModulo -= vFakeNInfRound;
					// cast back, removing all multiples of 2 pi, using
					// the trigonometric identity sin(x + 2n*pi) == sin(x)
					vModPhase -= (vModulo) * vPiTwo;
					// subtract pi to enther the polynomial's domain - [-pi, pi]
					vModPhase -= vPi;
					// initial polynomial
					vY = B * vModPhase + C * vModPhase * (vModPhase & vSignMask);
					// rotate vector ø (see domain)
					vPhase += vRotation;
					// newton step/increased precision (can be commented out)
					vY = P * (vY * (vY & vSignMask) - vY) + vY;
					// acquire input here to spread memory operations
					vIn = ArrayExtensions.GetVector(output, i);

					////////////////////////////////////////////////////////////
					// ADSR
					////////////////////////////////////////////////////////////
					// is attack still below the goal? if, mask out release and decay.
					vAttackStageMask = VectorOperations.CompareNotLessEqual(vattackStop, vAttack);
					// select attack or release/decay
					vADSR = vAttack & vAttackStageMask | VectorOperations.AndNot(vAttackStageMask, vVolume + vSustainOffset);
					// this ensures the coefficient is one (thus, not decreasing the currentvolume)
					// as long as attack has not reached the goal.
					vCoefficient = vAttackStageMask & vOne | VectorOperations.AndNot(vAttackStageMask, vFilterCoeff);
					// increase attack.
					vAttack += vRecipAttack;
					// decay / release:
					vVolume = vVolume * vCoefficient;

#if KILL_DENORMALS
					vVolume = vVolume & VectorOperations.CompareLessThan(vDenormalFlush, vVolume);
#endif
					////////////////////////////////////////////////////////////
					// Mixing
					////////////////////////////////////////////////////////////
					// select amplitude modulation coefficient:
					vMulC = vIn & vMulSelector | VectorOperations.AndNot(vMulSelector, vOne);
					// mix between additive/multiplicative mix of this operator and the old stuff
					// in the buffer
					vY = vY * vMulC * vMix * vADSR * vVol + vIn * vInvMix;
	
					// store output
					ArrayExtensions.SetVector(output, vY, i);
				}

				o.phase += nSamples * o.omega;
				// do some range reduction to avoid too many bits being thrown away inside loop
				o.phase -= (int)(MathExt.fPiTwoRec * o.phase) * MathExt.fPiTwo;

				// if attack mask is set, we can move on to decay. 
				// note, this is automatically selected in the loop.
				vAttackStageMask = VectorOperations.AndNot(vAttackStageMask, v4sf.One);
				if(e.stage == ADSREnvelope.Stage.Attack && (vAttackStageMask.W) > 0.0f)
				{
					e.stage = ADSREnvelope.Stage.Decay;
				}

				e.auxVol = vADSR.W;

				e.deltaSamples += nSamples;
				e.deltaSamples = e.deltaSamples > e.attackTime ? e.attackTime : e.deltaSamples;

			}

		}
		public void integerPhaseWrap()
		{
#if EXPERIMENTS
			// to be implemented.
			v4sf vControlMask;
			v4sf vRecDomain = SimdExt.vPiTwoRec;
			v4sf vDomain = SimdExt.vPiTwo;
			v4si viExponent;
			v4si viExponentBias = new v4si(127);
			v4si viExponentLow = new v4si(22);
			v4si viExponentShift = new v4si(23);
			v4si viFloor;
			v4sf vFloor;
			v4sf vPhase = v4sf.Zero;
			v4si viPhase;
			v4sf vFinalPhase = v4sf.Zero;

			int[] byteMask = new int[4];
			int[] phaseVal = new int[4];
			while(true)
			{
				viPhase = (v4si)vPhase;
				// this sets the mask if we should discard the result
				vControlMask = vPhase.CompareLessThan(vDomain);
				ArrayExtensions.SetVector(phaseVal, viPhase, 0);
				// get the unbiased exponent
				viExponent = VectorOperations.LogicalRightShift(viPhase, 23) - viExponentBias;
				// this will discard the value if there is no fractional bits.
				vControlMask = vControlMask | (v4sf)VectorOperations.CompareGreaterThan(viExponent, viExponentLow);
				// viFloor now holds the mount of unneeded bits
				viFloor = viExponentShift - viExponent;
				// we remove those by shifting left, and then back. can also be done using a mask.
				// however, since Mono.Simd doesn't support individual lane shifting,
				// we have to do this part in the integer unit.
				ArrayExtensions.SetVector(byteMask, viFloor, 0);
				// do the shifting
				byteMask[0] = (int)(((uint)phaseVal[0] >> byteMask[0]) << byteMask[0]);
				byteMask[1] = (int)(((uint)phaseVal[1] >> byteMask[1]) << byteMask[1]);
				byteMask[2] = (int)(((uint)phaseVal[2] >> byteMask[2]) << byteMask[2]);
				byteMask[3] = (int)(((uint)phaseVal[3] >> byteMask[3]) << byteMask[3]);


				viFloor = ArrayExtensions.GetVector(byteMask, 0);

				// convert back and select value.
				vFinalPhase = ((vPhase - (v4sf)viFloor) * vDomain) & vControlMask;
				vFinalPhase = vFinalPhase | VectorOperations.AndNot(vControlMask, vPhase);
			}

#endif
		}

	}



}
